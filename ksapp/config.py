# -*- coding: utf-8 -*-

import os

APP_NAME = 'ksapp'

class BaseConfig(object):

    DEBUG = False
    TESTING = False

    # os.urandom(24)
    SECRET_KEY = 'q\xf2\x92\xc4\x17-c\x1a\x83.k\xb4Z\x96K\xd5\x19|\xa9~\xb0\xbf\x8b\xc8'

    # SERVER_NAME = 'pinjot.com'
        

class DefaultConfig(BaseConfig):

    DEBUG = True

    SQLALCHEMY_ECHO = True
    # heroku (PostgreSQL)
    # if testing local, run command 'export DATABASE_URL=postgres://chris@localhost:5432/ksapp'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')

    # redis server
    REDIS_URL = os.getenv('REDISCLOUD_URL', 'redis://localhost:6379/0')

    # To create log folder.
    # $ sudo mkdir -p /var/log/ksapp
    # $ sudo chown $USER /var/log/ksapp
    DEBUG_LOG = '/var/log/ksapp/debug.log'

    # Email (Flask-email)
    # https://bitbucket.org/danjac/flask-mail/issue/3/problem-with-gmails-smtp-server
    MAIL_DEBUG = DEBUG
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = 'chrickso'
    MAIL_PASSWORD = 'test'
    DEFAULT_MAIL_SENDER = '%s@gmail.com' % MAIL_USERNAME


class TestConfig(BaseConfig):
    TESTING = True
    CSRF_ENABLED = False

    SQLALCHEMY_ECHO = False
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
