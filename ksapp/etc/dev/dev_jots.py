# -*- coding: utf-8 -*-

from collections import namedtuple

Jot = namedtuple('Jot', ['id', 'shelf', 'book', 'title', 'body', 'attachments'])

dev_jots = [

    Jot(0, 'Bitcoin', 'Securities Trading', '', 'getting started:', ['http://www.reddit.com/r/Bitcoin/comments/1jc38p/buying_btc_vs_mining_btc/cbd7hcn']),
    Jot(1, 'Blogs', 'Bushcraft / Survival / Outdoors', '', '', ['http://willowhavenoutdoor.com']),
    Jot(2, 'Cool Stuff', 'Watch / Read Later', '', '', []),
    Jot(3, 'Cutlery', 'Knives', '', 'Good recommendations for \'Budget\' knives:', ['http://www.cheftalk.com/t/70719/budget-knives-to-complement-current-equipment']),
    Jot(4, 'Cutlery', 'Sharpening', '', 'bdl technique:', ['http://www.cheftalk.com/t/44017/picking-a-knife']),
    Jot(5, 'Cutlery', 'Sharpening', '', '#2', ['http://www.cheftalk.com/t/73189/sharpening-mac-knives']),
    Jot(6, 'Cutlery', 'Sharpening', '', 'hone:', ['http://www.chefknivestogo.com/sharpeningrod.html']),
    Jot(7, 'Cutlery', 'Sharpening', '', 'recommended combo stone:', ['http://www.japaneseknifeimports.com/sharpening-supplies/gesshin-1000-6000-combo-stone.html']),
    Jot(8, 'Cutlery', 'Sharpening', '', 'post #9 has links and recommendations to whetstone sharpening tutorials:', ['http://www.cheftalk.com/t/76639/same-noo-b-question-with-a-twist']),
    Jot(9, 'Design', 'Colors', '', '', ['http://flatuicolors.com']),
    Jot(10, 'Design', 'Colors', '', '', ['http://www.colourlovers.com/palettes']),
    Jot(11, 'Design', 'Colors', '', 'The Colors of Dribbble color picker', ['http://nathanspeller.com/color-pickers/']),
    Jot(12, 'Design', 'Flat', '', '', ['http://designmodo.com/flat-design-principles/']),
    Jot(13, 'Design', 'Icons', '', '', ['http://ionicons.com']),
    Jot(14, 'Design', 'Tools / Progs / Apps', '', 'Sketch program looks great for UI dev:', ['https://medium.com/design-ux/25545f6cb161']),
    Jot(15, 'Design', 'UI', '', 'Interface Patterns:', ['http://pttrns.com/categories/13-navigations']),
    Jot(16, 'Design', 'UI', '', 'Usability Checklist:', ['http://userium.com']),
    Jot(17, 'Design', 'UI', '', 'how to radically simplify your UI:', ['http://nathanbarry.com/judo-application-design/?buffer_share=d7dd3&amp;utm_source=buffer&amp;utm_medium=facebook&amp;utm_campaign=Buffer%253A%252BNathan%2520Barry%252Bon%252Bfacebook']),
    Jot(18, 'Design', 'unshelved', '', 'google material design', ['http://googledevelopers.blogspot.com.au/2014/10/updated-material-design-guidelines-and.html?m=1', 'http://google.github.io/material-design-icons/']),
    Jot(19, 'Fitness', 'Bodyweight', '', 'Bodyweight routine w/ videos', ['http://bodyweightfitness.routinemorning.com']),
    Jot(20, 'Fitness', 'Kettlebell', '', 'RKC minimum program:s', ['http://www.reddit.com/r/kettlebell/comments/1bxcnu/kind_of_a_beginner_and_needing_help_with_a_routine/']),
    Jot(21, 'Gallbladder', 'Attacks', '', '', ['']),
    Jot(22, 'Gear', 'Binoculars', '', 'Steiner 8x30 Predator Pro http://www.opticsplanet.com/steiner-8x30-predator-pro-binoculars-288.html $229', ['']),
    Jot(23, 'Gear', 'Camping', '', 'Reddit ultralite gear lists:', ['https://docs.google.com/spreadsheet/lv?key=0AjT42yAEwdOOdGJ1UnVkNzNSSWwta09ZQnltbEtmNkE#gid=0', 'https://docs.google.com/spreadsheet/lv?key=0As-hvbUBJ_X_dHJXU3ZDb2VlOElUczlWS3JaUmNtNGc&authkey=CLCE2LAO&hl=en&authkey=CLCE2LAO#gid=70']),
    Jot(24, 'Gear', 'Gear Lists', '', '', ['https://docs.google.com/spreadsheet/lv?key=0Ahzu4AG8YuX2dFZoaEpDS3RJQ1ZFQ1NqSWFnSjJ0akE&pli=1#gid=0']),
    Jot(25, 'Gear', 'Water', '', 'Use Sawyer Squeeze with hanging bag for gravity filter:', ['http://www.bikebagshop.com/ortlieb-water-bags-c-36.html']),
    Jot(26, 'Health', 'Gallbladder', '', 'mega info cache:', ['http://en.m.wikipedia.org/wiki/Comparison_of_web_browsers#Image_format_support']),
    Jot(27, 'Health', 'Gallbladder', '', 'lots of good links:', ['http://balancedbites.com/2012/12/faqs-how-can-i-eat-paleo-without-a-gallbladder.html']),
    Jot(28, 'Health', 'Gallbladder', '', 'Food allergy linked to gallstones:', ['http://www.gallbladderattack.com/allergydiet.shtml']),
    Jot(29, 'Health', 'Products', '', 'Sunscreen: micronized zinc oxide based sunscreens', ['http://drmikehart.tumblr.com/post/51023794354/is-your-sunscreen-causing-cancer']),
    Jot(30, 'Valuable Information', 'Insurance', '', 'Hey OP... I used to be the guy who worked for insurance companies, and determined the value of every little thing in your house. The guy who would go head-to-head with those fire-truck-chasing professional loss adjusters. I may be able to help you not get screwed when filing your claim.' + \
                                                     'Our goal was to use the information you provided, and give the lowest damn value we can possibly justify for your item.' + \
                                                     'For instance, if all you say was "toaster" -- we would come up with a cheap-as-fuck $4.88 toaster from Walmart, meant to toast one side of one piece of bread at a time. And we would do that for every thing you have ever owned. We had private master lists of the most commonly used descriptions, and what the cheapest viable replacements were. We also had wholesale pricing on almost everything out there, so really scored cheap prices to quote. To further that example:' + \
                                                     '• If you said "toaster - $25" , we would have to be within -20% of that... so, we would find something that\'s pretty much dead-on $20.01.' + \
                                                     '• If you said "toaster- $200" , we\'d kick it back and say NEED MORE INFO, because that''s a ridiculous price for a toaster (with no other information given.)' + \
                                                     '• If you said "toaster, from Walmart" , you''re getting that $4.88 one.' + \
                                                     '• If you said "toaster, from Macys" , you''d be more likely to get a $25-35 one.' + \
                                                     '• If you said "toaster", and all your other kitchen appliances were Jenn Air / Kitchenaid / etc., you would probably get a matching one.' + \
                                                     '• If you said "Proctor Silex 42888 2-Slice Toaster from Wamart, $9", you just got yourself $9.' + \
                                                     '• If you said "High-end Toaster, Stainless Steel, Blue glowing power button" ... you might get $35-50 instead. We had to match all features that were listed.' + \
                                                     'I\'m not telling you to lie on your claim. Not at all. That would be illegal, and could cause much bigger issues (i.e., invalidating the entire claim). But on the flip side, it''s not always advantageous to tell the whole truth every time. Pay attention to those last two examples.' + \
                                                     'I remember one specific customer... he had some old, piece of shit projector (from mid-late 90s) that could stream a equally piece of shit consumer camcorder. Worth like $5 at a scrap yard. It had some oddball fucking resolution it could record at, though -- and the guy strongly insisted that we replace with "Like Kind And Quality" (trigger words). Ended up being a $65k replacement, because the only camera on the market happened to be a high-end professional video camera (as in, for shooting actual movies). $65-goddam-thousand-dollars because he knew that loophole, and researched his shit.' + \
                                                     'Remember to list fucking every -- even the most mundane fucking bullshit you can think of. For example, if I was writing up the shower in my bathroom:' + \
                                                     '• Designer Shower Curtain - $35' + \
                                                     '• Matching Shower Curtain Liner for Designer Shower Curtain - $15' + \
                                                     '• Shower Curtain Rings x20 - $15' + \
                                                     '• Stainless Steel Soap Dispenser for Shower - $35' + \
                                                     '• Natural Sponge Loofah - from Whole Foods - $15' + \
                                                     '• Natural Sponge Loofah for Back - from Whole Foods - $19' + \
                                                     '• Holder for Loofahs - $20' + \
                                                     '• Bars of soap - from Lush - $12 each (qty: 4)' + \
                                                     '• Bath bomb - from Lush - $12' + \
                                                     '• High end shampoo - from salon - $40' + \
                                                     '• High end conditioner - from salon - $40' + \
                                                     '• Refining pore mask - from salon - $55' + \
                                                     'I could probably keep thinking, and bring it up to about $400 for the contents of my shower. Nothing there is "unreasonable" , nothing there is clearly out of place, nothing seems obviously fake. The prices are a little on the high-end, but the reality is, some people have expensive shit -- it won''t actually get questioned. No claims adjuster is going to bother nitpicking over the cost of fucking Lush bath bombs, when there is a 20,000 item file to go through. The adjuster has other shit to do, too.' + \
                                                     'Most people writing claims for a total loss wouldn\'t even bother with the shower (it\'s just some used soap and sponges..) -- and those people would be losing out on $400.' + \
                                                     'Some things require documentation & ages. If you say "tv - $2,000" -- you\'re getting a 32" LCD, unless you can provide it was from the last year or two w/ receipts. Hopefully you have a good paper trail from credit/debit card expenditure / product registrations / etc.' + \
                                                     'If you\'re missing paper trails for things that were legitimately expensive -- go through every photo you can find that was taken in your house. Any parties you may have thrown, and guests put pics up on Facebook. Maybe an Imgur photo of your cat, hiding under a coffee table you think you purchased from Restoration Hardware. Like... seriously... come up with any evidence you possibly can, for anything that could possibly be deemed expensive.' + \
                                                     'The fire-truck chasing loss adjusters are evil sons of bitches, but, they actually do provide some value. You will definitely get more money, even if they take a cut. But all they\'re really doing, is just nitpicking the ever-living-shit out of everything you possibly owned, and writing them all up "creatively" for the insurance company to process.' + \
                                                     'Sometimes people would come back to us with "updated* claims. They tried it on their own, and listed stuff like "toaster", "microwave", "tv" .. and weren\'t happy with what they got back. So they hired a fire-truck chaser, and re-submitted with "more information." I have absolutely seen claims go from under $7k calculated, to over $100k calculated. (It\'s amazing what can happen when people suddenly "remember" their entire wardrobe came from Nordstrom.)', ['']),
    Jot(31, 'Keepshelf', 'Eventually Implement', '', 'add \'delete selected\' options to shelf/book/jot listing.in settings > email: have options \'When can Keepshelf e-mail you?\' with selection Confirm Mass Deletions Via E-mail. (mass deletions are any time more than x # of things is getting removed at once)', ['']),
    Jot(32, 'Keepshelf', 'Functionality Tutorials', '', 'basic search with stock postgres', ['']),
    Jot(33, 'Keepshelf', 'How Stuff Works', '', 'Activity Stream: If logged in, homepage will default to the \'following\' stream', ['']),
    Jot(34, 'Keepshelf', 'How Stuff Works', '', 'Renaming Shelves & Books: Switch so that 1 function is called to make updates instead of determining which of3 based on name and/or description', ['']),
    Jot(35, 'Keepshelf', 'How Stuff Works', '', 'Sidebar shelf/book list: The sidebar will be switched to default to showing an alphabetized list of "pin\'d" shelves/books. Maybe a link to switch between pin\'d/recent. Admin\'ing the pin\'d listing will be done via checkboxes next to each entry on the shelf and book listing pages.', ['']),
    Jot(36, 'Keepshelf', 'How Stuff Works', '', 'Delete Shelf: Deleting a shelf should delete all books and jots inside of it. A warning will be displayed recommending to move books or jots out before deleting.', ['']),
    Jot(37, 'Keepshelf', 'Logo Ideas', '', '', ['http://logofaves.com/highest-rated-logos/']),
    Jot(38, 'Keepshelf', 'Marketing', '', 'KeepShelf is a social note/url saving/sharing web application.', ['']),
    Jot(39, 'Keepshelf', 'Marketing', '', 'KeepShelf is how you save the internet. KeepShelf is how/where you store and organize thoughts, links, and files.', ['']),
    Jot(40, 'Keepshelf', 'Marketing', '', 'KeepShelf: The solutuon to emailing yourself. / Stop emailing yourself.', ['']),
    Jot(41, 'Keepshelf', 'Marketing', '', 'quip: pinterest utility meets craigslist simplicity', ['']),
    Jot(42, 'Keepshelf', 'Marketing', '', 'launch announcements:', ['https://news.ycombinator.com/item?id=6488822']),
    Jot(43, 'Keepshelf', 'Marketing', '', '', ['http://www.happybootstrapper.com/2013/my-almost-failed-launch/']),
    Jot(44, 'Keepshelf', 'Marketing', '', 'good stuff on landing page techniques:', ['https://medium.com/what-i-learned-building/63fa3e5603b']),
    Jot(45, 'Keepshelf', 'Marketing', '', 'Keepshelf lets you collect/organize your thoughts and browse other libraries for ideas.', ['']),
    Jot(46, 'Keepshelf', 'Marketing', '', 'lifetime free membership to the first 100 signups', ['']),
    Jot(47, 'Keepshelf', 'Marketing', '', 'Keepshelf: Collect/organize/find/share jots.', ['']),
    Jot(48, 'Keepshelf', 'Marketing', '', 'Keepshelf: Collect and organize your jots.', ['']),
    Jot(49, 'Keepshelf', 'Memberships', '', 'memberships will enable you to mark your shelves/books/jots as \'private\', making them invisible and inaccessible to anyone but the account owner. if membership is not renewed, all "private" entries will remain private but you can no longer edit existing or create new private objects. editing a private object without an active membership will turn it public (a warning will be displayed to confirm this).', ['']),
    Jot(50, 'Keepshelf', 'Revenue Ideas', '', '$3.99 / month for premium account: - private shelves / books / jots', ['']),
    Jot(51, 'Keepshelf', 'Site Design', '', 'image format support per browser:', ['http://en.m.wikipedia.org/wiki/Comparison_of_web_browsers#Image_format_support']),
    Jot(52, 'Keepshelf', 'Site Design', '', 'resizing images via css:', ['http://stackoverflow.com/questions/2076284/scaling-images-proportionally-in-css-with-max-width']),
    Jot(53, 'Keepshelf', 'Site Design', '', 'Off-canvas layout (for mobile)', ['http://jpanelmenu.com/']),
    Jot(54, 'Keepshelf', 'Slogan Ideas', '', 'Store your thoughts.', ['']),
    Jot(55, 'Keepshelf', 'Slogan Ideas', '', 'Keepshelf: Your thoughts collected/organized.', ['']),
    Jot(56, 'Keepshelf', 'Slogan Ideas', '', 'Keepshelf: Collecting Your Memories', ['']),
    Jot(57, 'Keepshelf', 'Slogan Ideas', '', 'Keepshelf: Collecting Your Memories', ['']),
    Jot(58, 'Keepshelf', 'To Do', '', 'links to reddits scraper file:', ['http://www.crummy.com/software/BeautifulSoup/']),
    Jot(59, 'Keepshelf', 'To Do', '', 'get title from provided URL for attachment: ', ['https://www.google.com/search?q=get+page+title+from+url+with+python&oq=get+page+title+from+url+with+python&aqs=chrome.0.57j0j62&client=chrome-mobile&sourceid=chrome-mobile&espv=1&ie=UTF-8']),
    Jot(60, 'Keepshelf', 'To Do', '', 'add \'rejot\' link to book details jot listing and make \'edit\' and \'delete\' visible only to the jot owner', ['']),
    Jot(61, 'Keepshelf', 'To Do', '', 'implement rate limiting:', ['http://www.codinghorror.com/blog/2009/02/rate-limiting-and-velocity-checking.html']),
    Jot(62, 'Keepshelf', 'To Do', '', 'update to latest itsdangerous:', ['http://pythonhosted.org/itsdangerous/']),
    Jot(63, 'Keepshelf', 'To Do', '', 'add \'Library\' link to the sidebar that will be how you access other user\'s profiles, books, and shelves', ['']),
    Jot(64, 'Keepshelf', 'To Do', '', 'set up redis-queue for long jobs: http://python-rq.org', ['']),
    Jot(65, 'Keepshelf', 'To Do', '', 'get activity stream working', ['']),
    Jot(66, 'Keepshelf', 'To Do', '', 'get edit and delete links working for shelfs and books', ['']),
    Jot(67, 'Marketing', 'Colors', '', 'color/emotions write up:', ['http://www.ma-no.org/en/content/index_why-is-facebook-blue-the-science-behind-colors-in-marketing_1498.php']),
    Jot(68, 'Marketing', 'Keepshelf', '', 'Keepshelf: Personal Data Organization', ['']),
    Jot(69, 'Marketing', 'Keepshelf', '', 'Keepshelf: A better way to store your data.', ['']),
    Jot(70, 'Philosophy ', 'Stoicism', '', '', ['http://sivers.org/book/StoicJoy']),
    Jot(71, 'Phones', 'Android', '', 'ok to freeze spreadsheet:', ['https://docs.google.com/spreadsheet/lv?key=0AsYpcz4odTbEdHlsT0syalBqT2kxWlVoNnhTNV84YUE']),
    Jot(72, 'Phones', 'Android', '', """htc one m8:
                                        One of the things that's being overlooked about the M8 is it's camera. While in stock form I must admit it's a mess for outdoor landscapes but If you're willing to root you're phone & install Jishnu Sur's HTC One Cam V3.zip mod it corrects most of it's shortcomings. it however doesn't stop there. For even more camra awsomeness, a trip over to Google play to pick up a litle app called Camera FV-5 will have you churning out 4.5 MB Raw .PNG files that will bitch slap every one of it's rivals in their stock form. The ultra pixel Camra HTC hyped up is actualy there. You just have to do a little digging to bring it out. And lets also not forget the sound. as is it's awsome. By simply rooting the phone and flashing the rom you get a choice of either Beats audio (which I prefer) or the Htc one Harman Kardon presets.
                                        Most will likely go for the Harman Kardon presets but as a true audiofile I can tell you first hand that while not as spacious sounding (I feel Harman Kardon over did it with the brightness) Beats audio introduces a warm, rich, ampli spacious quality sound with Bass the likes I have never heard coming from such a small device.
                                        Another one of the HTC's strong points are it's actual data thruput speeds. whether it be with wi-fi or 4G LTE. The phone is one of the fastest out there.
                                        So the bottom line is, If you like to root your device you may want to go for the M8 & if done right your images won't be pixelated and you also wont have a problem with its dynamic range.
                                        The M8 Rooted is just beautifully awesome.""", ['']),
    Jot(73, 'Phones ', 'Android', '', 'root and Greenify to remove bloatware', ['']),
    Jot(74, 'Phones ', 'Google Cardboard', '', '', ['http://www.reddit.com/r/Android/comments/2gddbl/google_cardboard_366_free_shipping_from_tinydeal/cki0ecn']),
    Jot(75, 'Phones ', 'Google Cardboard', '', '', ['https://www.reddit.com/r/AndroidGaming/comments/2g98b7/google_cardboard_virtual_reality_for_about_20/cktwxva']),
    Jot(76, 'Phones ', 'security', '', 'snowden recommended call/text apps:', ['https://whispersystems.org']),
    Jot(77, 'PinJot', 'Functionality Ideas', '', 'Reaching below a certain score should automatically remove the jot from listings/deactivate', ['']),
    Jot(78, 'PinJot', 'Slogan Ideas', '', 'pinjot: get it out there', ['']),
    Jot(79, 'PinJot', 'Slogan Ideas', '', 'PinJot: Write to the Internet.', ['']),
    Jot(80, 'Privacy', 'Google', '', '', ['https://medium.com/productivity-in-the-cloud/6-links-that-will-show-you-what-google-knows-about-you-f39b8af9decc']),
    Jot(81, 'Privacy', 'Passwords', '', 'Diceware is a method of using a dice to select 6 random words from a word list:', ['http://world.std.com/~reinhold/diceware.html']),
    Jot(82, 'Privacy', 'Passwords', '', 'Lastpass and KeePass are software to store various passwords securely:', ['https://lastpass.com/create_account.php?fromloginpage=1', 'http://keepass.info/']),
    Jot(83, 'Programming', 'Python / Flask / Redis', '', 'flask TDD tutorial:', ['https://github.com/mjhea0/flaskr-tdd/blob/master/README.md']),
    Jot(84, 'Programming', 'Python / Flask / Redis', '', 'redis-py syntax:', ['http://redis-py.readthedocs.org/en/latest/']),
    Jot(85, 'Random', 'Awesome', '', 'Home made Spiderman costume:', ['http://imgur.com/a/xfpPm']),
    Jot(86, 'Read Later', 'Non-mobile', '', 'python/android app dev:', ['https://speakerdeck.com/pyconca/android-the-land-that-python-forgot-christopher-neugebauer']),
    Jot(87, 'Recipes', 'Kale', '', 'Tim Ferris\' kale chips: ', ['http://leitesculinaria.com/85391/recipes-kale-chips.html']),
    Jot(88, 'Recipes', 'Low Carb', '', 'ultra low carb puppy chow:', ['http://athlete.io/6103/ultra-low-carb-puppy-chow/']),
    Jot(89, 'Reference', 'Repair / Replace', '', '\'05 Suzuki Reno headlight replacement:', ['http://www.suzuki-forums.com/suzuki-forenza-reno-forum/39505-changing-headlight-bulbs-reno-05-a.html']),
    Jot(90, 'Reference', 'Reference', '', 'layout/design reference', ['http://www.careercup.com/resume']),
    Jot(91, 'Show People', 'Evan', '', 'link drawn like different cartoon styles:', ['http://imgur.com/a/rnfvJ']),
    Jot(92, 'Show People', 'Evan', '', 'cool zelda art:', ['http://uniquelegend.deviantart.com/art/Majora-s-Mask-The-Transformation-345175231']),
    Jot(93, 'Show People', 'Evan', '', 'Students drawings:', ['http://imgur.com/a/F9kbH']),
    Jot(94, 'Show People', 'Kristina', '', 'celeb before/after photoshop', ['http://imgur.com/a/zRmEM']),
    Jot(95, 'Startups', 'Advice/Good Ideas/Getting Started', '', '', ['http://www.reddit.com/r/Entrepreneur/comments/2h1mlt/the_inner_workings_of_a_subscription_box_company/']),
    Jot(96, 'The Next Big Thing', 'Good Articles', '', 'Platform Thinking:', ['http://platformed.info/how-to-become-a-billion-dollar-startup-airbnb-youtube-and-platform-thinking/']),
    Jot(97, 'The Next Big Thing', 'Good Articles', '', 'The Next Facebook', ['https://medium.com/musings-about-text-boxes/8157c364d26a']),
    Jot(98, 'Tips/Tricks', 'Python', '', 'Python cheet-sheet:', ['http://overapi.com/python/']),
    Jot(99, 'Tips/Tricks', 'Redis', '', 'reduce memory usage:', ['http://davidcel.is/blog/2013/03/20/the-story-of-my-redis-database/']),
    Jot(100, 'To-do', 'Keepshelf', '', 'setting: open jot links in a new window', ['']),
    Jot(101, 'To-do', 'Keepshelf', '', 'adjust buttons rounded corners to 2px', ['']),
    Jot(102, 'To-do', 'Keepshelf', '', 'adjust buttons rounded corners to 2px', ['']),
    Jot(103, 'Web Dev', 'Front End', '', 'flat css framework:', ['http://icalialabs.github.io/furatto/index.html']),

]


State = namedtuple('State', ['id', 'name', 'abbrev', 'url', 'city_list'])
usa = [

        State(555, 'www', 'www', 'wwww',[]), # for local device testing

        State(0, 'minimac', 'mm', '<invalid>',[]), # for local device testing

        State(1, 'Alabama', 'AL', 'alabama', [
                        ['AL01', 'Auburn', 'auburn'],
                        ['AL02', 'Birmingham', 'bham'],
                        ['AL03', 'Dothan', 'dothan'],
                        ['AL04', 'Florence / Muscle Shoals', 'shoals'],
                        ['AL05', 'Gadsden / Anniston', 'gadsden'],
                        ['AL06', 'Huntsville / Decatur', 'huntsville'],
                        ['AL07', 'Mobile', 'mobile'],
                        ['AL08', 'Montgomery', 'montgomery'],
                        ['AL09', 'Tuscaloosa', 'tuscaloosa']
                        ]),
        State(2, 'Alaska', 'AK', 'alaska', [
                        ['AK01', 'Anchorage / Mat-su', 'anchorage'],
                        ['AK02', 'Fairbanks', 'fairbanks'],
                        ['AK03', 'Kenai Peninsula', 'kenai'],
                        ['AK04', 'Southeast Alaska', 'juneau']
                        ]),
        State(3, 'Arizona', 'AZ', 'arizona', [
                        ['AZ01', 'Flagstaff / Sedona', 'flagstaff'],
                        ['AZ02', 'Mohave County', 'mohave'],
                        ['AZ03', 'Phoenix', 'phoenix'],
                        ['AZ04', 'Prescott', 'prescott'],
                        ['AZ05', 'Show Low', 'showlow'],
                        ['AZ06', 'Sierra Vista', 'sierravista'],
                        ['AZ07', 'Tucson', 'tucson'],
                        ['AZ08', 'Yuma', 'yuma']
                        ]),
        State(4, 'Arkansas', 'AR', 'arkansas', [
                        ['AR01', 'Fayetteville', 'fayar'],
                        ['AR02', 'Fort Smith', 'fortsmith'],
                        ['AR03', 'Jonesboro', 'jonesboro'],
                        ['AR04', 'Little Rock', 'littlerock'],
                        ['AR05', 'Texarkana', 'texarkana']
                        ]),
        State(5, 'California', 'CA', 'california', [
                        ['CA01', 'Bakersfield', 'bakersfield'],
                        ['CA02', 'Chico', 'chico'],
                        ['CA03', 'Fresno / Madera', 'fresno'],
                        ['CA04', 'Gold Country', 'goldcountry'],
                        ['CA05', 'Hanford-Corcoran', 'hanford'],
                        ['CA06', 'Humboldt County', 'humboldt'],
                        ['CA07', 'Imperial County', 'imperial'],
                        ['CA08', 'Inland Empire', 'inlandempire'],
                        ['CA09', 'Los Angeles', 'losangeles'],
                        ['CA10', 'Mendocino County', 'mendocino'],
                        ['CA11', 'Merced', 'merced'],
                        ['CA12', 'Modesto', 'modesto'],
                        ['CA13', 'Monterey Bay', 'monterey'],
                        ['CA14', 'Orange County', 'orangecounty'],
                        ['CA15', 'Palm Springs', 'palmsprings'],
                        ['CA16', 'Redding', 'redding'],
                        ['CA17', 'Sacramento', 'sacramento'],
                        ['CA18', 'San Diego', 'sandiego'],
                        ['CA19', 'San Francisco Bay Area', 'sfbay'],
                        ['CA20', 'San Luis Obispo', 'slo'],
                        ['CA21', 'Santa Barbara', 'santabarbara'],
                        ['CA22', 'Santa Maria', 'santamaria'],
                        ['CA23', 'Siskiyou County', 'siskiyou'],
                        ['CA24', 'Stockton', 'stockton'],
                        ['CA25', 'Susanville', 'susanville'],
                        ['CA26', 'Ventura County', 'ventura'],
                        ['CA27', 'Visalia-Tulare', 'visalia'],
                        ['CA28', 'Yuba-Sutter', 'yubasutter']
                        ]),
        State(6, 'Colorado', 'CO', 'colorado', [
                        ['CO01', 'Boulder', 'boulder'],
                        ['CO02', 'Colorado Springs', 'cosprings'],
                        ['CO03', 'Denver', 'denver'],
                        ['CO04', 'Eastern CO', 'eastco'],
                        ['CO05', 'Fort Collins / North CO', 'fortcollins'],
                        ['CO06', 'High Rockies', 'rockies'],
                        ['CO07', 'Pueblo', 'pueblo'],
                        ['CO08', 'Western Slope', 'westslope']
                        ]),
        State(7, 'Connecticut', 'CT', 'connecticut', [
                        ['CT01', 'Eastern CT', 'newlondon'],
                        ['CT02', 'Hartford', 'hartford'],
                        ['CT03', 'New Haven', 'newhaven'],
                        ['CT04', 'Northwest CT', 'nwct']
                        ]),
        State(8, 'Delaware', 'DE', 'delaware', [
                        ['DE01', 'dover', 'dover'],
                        ['DE02', 'wilmington', 'wilmington']
                        ]),
        State(9, 'District of Columbia', 'DC', 'dc', [
                        ['DC01', 'Washington', 'washingtondc']
                        ]),
        State(10, 'Florida', 'FL', 'florida', [
                        ['FL01', 'Daytona Beach', 'daytona'],
                        ['FL02', 'florida keys', 'keys'],
                        ['FL03', 'fort lauderdale', 'fortlauderdale'],
                        ['FL04', 'FT Myers / SW Florida', 'fortmyers'],
                        ['FL05', 'gainesville', 'gainesville'],
                        ['FL06', 'heartland florida', 'cfl'],
                        ['FL07', 'jacksonville', 'jacksonville'],
                        ['FL08', 'lakeland', 'lakeland'],
                        ['FL09', 'North Central FL', 'lakecity'],
                        ['FL10', 'ocala', 'ocala'],
                        ['FL11', 'okaloosa / walton', 'okaloosa'],
                        ['FL12', 'orlando', 'orlando'],
                        ['FL13', 'panama city', 'panamacity'],
                        ['FL14', 'pensacola', 'pensacola'],
                        ['FL15', 'sarasota-bradenton', 'sarasota'],
                        ['FL16', 'south florida', 'miami'],
                        ['FL17', 'space coast', 'spacecoast'],
                        ['FL18', 'St. augustine', 'staugustine'],
                        ['FL19', 'tallahassee', 'tallahassee'],
                        ['FL20', 'tampa bay area', 'tampa'],
                        ['FL21', 'treasure coast', 'treasure'],
                        ['FL21', 'west palm beach', 'westpalmbeach']
                        ]),
        State(11, 'Georgia', 'GA', 'georgia', [
                        ['GA01', 'albany', 'albanyga'],
                        ['GA02', 'athens', 'athensga'],
                        ['GA03', 'atlanta', 'atlanta'],
                        ['GA04', 'augusta', 'augusta'],
                        ['GA05', 'brunswick', 'brunswick'],
                        ['GA06', 'columbus', 'columbusga'],
                        ['GA07', 'macon / warner robins', 'macon'],
                        ['GA08', 'northwest GA', 'nwga'],
                        ['GA09', 'savannah / hinesville', 'savannah'],
                        ['GA10', 'statesboro', 'statesboro'],
                        ['GA11', 'valdosta', 'valdosta']
                        ]),
        State(12, 'Hawaii', 'HI', 'hawaii', [
                        ['HI01', 'honolulu', 'honolulu']
                        ]),
        State(13, 'Idaho', 'ID', 'idaho', [
                        ['ID01', 'boise', 'boise'],
                        ['ID02', 'east idaho', 'eastidaho'],
                        ['ID03', 'lewiston / clarkston', 'lewiston'],
                        ['ID04', 'twin falls', 'twinfalls']
                        ]),
        State(14, 'Illinois', 'IL', 'illinois', [
                        ['IL01', 'Bloomington-Normal', 'bn'],
                        ['IL02', 'champaign urbana', 'chambana'],
                        ['IL03', 'chicago', 'chicago'],
                        ['IL04', 'decatur', 'decatur'],
                        ['IL05', 'la salle co', 'lasalle'],
                        ['IL06', 'Mattoon-Charleston', 'mattoon'],
                        ['IL07', 'peoria', 'peoria'],
                        ['IL08', 'rockford', 'rockford'],
                        ['IL09', 'southern illinois', 'carbondale'],
                        ['IL10', 'springfield', 'springfieldil'],
                        ['IL11', 'western IL', 'quincy']
                        ]),
        State(15, 'Indiana', 'IN', 'indiana', [
                        ['IN01', 'bloomington', 'bloomington'],
                        ['IN02', 'evansville', 'evansville'],
                        ['IN03', 'fort wayne', 'fortwayne'],
                        ['IN04', 'indianapolis', 'indianapolis'],
                        ['IN05', 'kokomo', 'kokomo'],
                        ['IN06', 'lafayette / west lafayette', 'tippecanoe'],
                        ['IN07', 'muncie / anderson', 'muncie'],
                        ['IN08', 'richmond', 'richmondin'],
                        ['IN09', 'south bend / michiana', 'southbend'],
                        ['IN10', 'terre haute', 'terrehaute']
                        ]),
        State(16, 'Iowa', 'IA', 'iowa', [
                        ['IA01', 'ames', 'ames'],
                        ['IA02', 'cedar rapids', 'cedarrapids'],
                        ['IA03', 'des moines', 'desmoines'],
                        ['IA04', 'dubuque', 'dubuque'],
                        ['IA05', 'fort dodge', 'fortdodge'],
                        ['IA06', 'iowa city', 'iowacity'],
                        ['IA07', 'mason city', 'masoncity'],
                        ['IA08', 'quad cities', 'quadcities'],
                        ['IA09', 'sioux city', 'siouxcity'],
                        ['IA10', 'southeast IA', 'ottumwa'],
                        ['IA11', 'waterloo / cedar falls', 'waterloo']
                        ]),
        State(17, 'Kansas', 'KS', 'kansas', [
                        ['KS01', 'lawrence', 'lawrence'],
                        ['KS02', 'manhattan', 'ksu'],
                        ['KS03', 'northwest KS', 'nwks'],
                        ['KS04', 'salina', 'salina'],
                        ['KS05', 'southeast KS', 'seks'],
                        ['KS06', 'southwest KS', 'swks'],
                        ['KS07', 'topeka', 'topeka'],
                        ['KS08', 'wichita', 'wichita']
                        ]),
        State(18, 'Kentucky', 'KY', 'kentucky', [
                        ['KY01', 'bowling green', 'bgky'],
                        ['KY02', 'eastern kentucky', 'eastky'],
                        ['KY03', 'lexington', 'lexington'],
                        ['KY04', 'louisville', 'louisville'],
                        ['KY05', 'owensboro', 'owensboro'],
                        ['KY06', 'western KY', 'westky']
                        ]),
        State(19, 'Louisiana', 'LA', 'louisiana', [
                        ['LA01', 'baton rouge', 'batonrouge'],
                        ['LA02', 'central louisiana', 'cenla'],
                        ['LA03', 'houma', 'houma'],
                        ['LA04', 'lafayette', 'lafayette'],
                        ['LA05', 'lake charles', 'lakecharles'],
                        ['LA06', 'monroe', 'monroe'],
                        ['LA07', 'new orleans', 'neworleans'],
                        ['LA08', 'shreveport', 'shreveport']
                        ]),
        State(20, 'Maine', 'ME', 'maine', [
                        ['ME01', 'portland', 'portlandme']
                        ]),
        State(21, 'Maryland', 'MD', 'maryland', [
                        ['MD01', 'annapolis', 'annapolis'],
                        ['MD02', 'baltimore', 'baltimore'],
                        ['MD03', 'eastern shore', 'easternshore'],
                        ['MD04', 'frederick', 'frederick'],
                        ['MD05', 'southern maryland', 'smd'],
                        ['MD06', 'western maryland', 'westmd']
                        ]),
        State(22, 'Massachusetts', 'MA', 'massachusetts', [
                        ['MA01', 'boston', 'boston'],
                        ['MA02', 'cape cod / islands', 'capecod'],
                        ['MA03', 'south coast', 'southcoast'],
                        ['MA04', 'western massachusetts', 'westernmass'],
                        ['MA05', 'worcester / central MA', 'worcester']
                        ]),
        State(23, 'Michigan', 'MI', 'michigan', [
                        ['MI01', 'ann arbor', 'annarbor'],
                        ['MI02', 'battle creek', 'battlecreek'],
                        ['MI03', 'central michigan', 'centralmich'],
                        ['MI04', 'detroit metro', 'detroit'],
                        ['MI05', 'flint', 'flint'],
                        ['MI06', 'grand rapids', 'grandrapids'],
                        ['MI07', 'holland', 'holland'],
                        ['MI08', 'jackson', 'jxn'],
                        ['MI09', 'kalamazoo', 'kalamazoo'],
                        ['MI10', 'lansing', 'lansing'],
                        ['MI11', 'monroe', 'monroemi'],
                        ['MI12', 'muskegon', 'muskegon'],
                        ['MI13', 'northern michigan', 'nmi'],
                        ['MI14', 'port huron', 'porthuron'],
                        ['MI15', 'Saginaw-Midland-Baycity', 'saginaw'],
                        ['MI16', 'southwest michigan', 'swmi'],
                        ['MI17', 'the thumb', 'thumb'],
                        ['MI18', 'upper peninsula', 'up']
                        ]),
        State(24, 'Minnesota', 'MN', 'minnesota', [
                        ['MN01', 'bemidji', 'bemidji'],
                        ['MN02', 'brainerd', 'brainerd'],
                        ['MN03', 'duluth / superior', 'duluth'],
                        ['MN04', 'mankato', 'mankato'],
                        ['MN05', 'minneapolis / st paul', 'minneapolis'],
                        ['MN06', 'rochester', 'rmn'],
                        ['MN07', 'southwest MN', 'marshall'],
                        ['MN08', 'st cloud', 'stcloud']
                        ]),
        State(25, 'Mississippi', 'MS', 'mississippi', [
                        ['MS01', 'gulfport / biloxi', 'gulfport'],
                        ['MS02', 'hattiesburg', 'hattiesburg'],
                        ['MS03', 'jackson', 'jackson'],
                        ['MS04', 'meridian', 'meridian'],
                        ['MS05', 'north mississippi', 'northmiss'],
                        ['MS06', 'southwest MS', 'natchez']
                        ]),
        State(26, 'Missouri', 'MO', 'missouri', [
                        ['MO01', 'columbia / jeff city', 'columbiamo'],
                        ['MO02', 'joplin', 'joplin'],
                        ['MO03', 'kansas city', 'kansascity'],
                        ['MO04', 'kirksville', 'kirksville'],
                        ['MO05', 'lake of the ozarks', 'loz'],
                        ['MO06', 'southeast missouri', 'semo'],
                        ['MO07', 'springfield', 'springfield'],
                        ['MO08', 'st joseph', 'stjoseph'],
                        ['MO09', 'st louis', 'stlouis']
                        ]),
        State(27, 'Montana', 'MT', 'Montana', [
                        ['MT01', 'billings', 'billings'],
                        ['MT02', 'bozeman', 'bozeman'],
                        ['MT03', 'butte', 'butte'],
                        ['MT04', 'great falls', 'greatfalls'],
                        ['MT05', 'helena', 'helena'],
                        ['MT06', 'kalispell', 'kalispell'],
                        ['MT07', 'missoula', 'missoula'],
                        ['MT08', 'montana (Old)', 'montana']
                        ]),
        State(28, 'Nebraska', 'NE', 'nebraska', [
                        ['NE01', 'grand island', 'grandisland'],
                        ['NE02', 'lincoln', 'lincoln'],
                        ['NE03', 'north platte', 'northplatte'],
                        ['NE04', 'omaha / council bluffs', 'omaha'],
                        ['NE05', 'scottsbluff / panhandle', 'scottsbluff']
                        ]),
        State(30, 'Nevada', 'NV', 'nevada', [
                        ['NV01', 'elko', 'elko'],
                        ['NV02', 'las vegas', 'lasvegas'],
                        ['NV03', 'reno / tahoe', 'reno']
                        ]),
        State(31, 'New Hampshire', 'NH', 'nh', [
                        ['NH01', 'manchester', 'manchester'],
                        ['NH02', 'nashua', 'nashua']
                        ]),
        State(32, 'New Jersey', 'NJ', 'newjersey', [
                        ['NJ01', 'central NJ', 'cnj'],
                        ['NJ02', 'jersey shore', 'jerseyshore'],
                        ['NJ03', 'north jersey', 'northjersey'],
                        ['NJ04', 'south jersey', 'southjersey']
                        ]),
        State(33, 'New Mexico', 'NM', 'newmexico', [
                        ['NM01', 'albuquerque', 'albuquerque'],
                        ['NM02', 'clovis / portales', 'clovis'],
                        ['NM03', 'farmington', 'farmington'],
                        ['NM04', 'las cruces', 'lascruces'],
                        ['NM05', 'roswell / carlsbad', 'roswell'],
                        ['NM06', 'santa fe / taos', 'santafe']
                        ]),
        State(34, 'New York', 'NY', 'newyork', [
                        ['NY01', 'albany', 'albany'],
                        ['NY02', 'binghamton', 'binghamton'],
                        ['NY03', 'buffalo', 'buffalo'],
                        ['NY04', 'catskills', 'catskills'],
                        ['NY05', 'chautauqua', 'chautauqua'],
                        ['NY06', 'Elmira-Corning', 'elmira'],
                        ['NY07', 'finger lakes', 'fingerlakes'],
                        ['NY08', 'glens falls', 'glensfalls'],
                        ['NY09', 'hudson valley', 'hudsonvalley'],
                        ['NY10', 'ithaca', 'ithaca'],
                        ['NY11', 'long island', 'longisland'],
                        ['NY12', 'new york city', 'newyorkny'],
                        ['NY13', 'oneonta', 'oneonta'],
                        ['NY14', 'Plattsburgh-Adirondacks', 'plattsburgh'],
                        ['NY15', 'Potsdam-Canton-Massena', 'potsdam'],
                        ['NY16', 'rochester', 'rochester'],
                        ['NY17', 'syracuse', 'syracuse'],
                        ['NY18', 'twin tiers NY/PA', 'twintiers'],
                        ['NY19', 'Utica-Rome-Oneida', 'utica'],
                        ['NY20', 'watertown', 'watertown']
                        ]),
        State(35, 'North Carolina', 'NC', 'northcarolina', [
                        ['NC01', 'asheville', 'asheville'],
                        ['NC02', 'boone', 'boone'],
                        ['NC03', 'charlotte', 'charlotte'],
                        ['NC04', 'eastern NC', 'eastnc'],
                        ['NC05', 'fayetteville', 'fayetteville'],
                        ['NC06', 'greensboro', 'greensboro'],
                        ['NC07', 'hickory / lenoir', 'hickory'],
                        ['NC08', 'jacksonville', 'onslow'],
                        ['NC09', 'outer banks', 'outerbanks'],
                        ['NC10', 'raleigh / durham / CH', 'raleigh'],
                        ['NC11', 'wilmington', 'wilmingtonnc'],
                        ['NC12', 'Winston-Salem', 'winstonsalem']
                        ]),
        State(36, 'North Dakota', 'ND', 'northdakota', [
                        ['ND01', 'bismarck', 'bismarck'],
                        ['ND02', 'fargo / moorhead', 'fargo'],
                        ['ND03', 'grand forks', 'grandforks'],
                        ['ND04', 'north dakota', 'nd']
                        ]),
        State(37, 'Ohio', 'OH', 'ohio', [
                        ['OH01', 'akron / canton', 'akroncanton'],
                        ['OH02', 'ashtabula', 'ashtabula'],
                        ['OH03', 'athens', 'athensohio'],
                        ['OH04', 'chillicothe', 'chillicothe'],
                        ['OH05', 'cincinnati', 'cincinnati'],
                        ['OH06', 'cleveland', 'cleveland'],
                        ['OH07', 'columbus', 'columbus'],
                        ['OH08', 'dayton / springfield', 'dayton'],
                        ['OH09', 'lima / findlay', 'limaohio'],
                        ['OH10', 'mansfield', 'mansfield'],
                        ['OH11', 'sandusky', 'sandusky'],
                        ['OH12', 'toledo', 'toledo'],
                        ['OH13', 'tuscarawas co', 'tuscarawas'],
                        ['OH14', 'youngstown', 'youngstown'],
                        ['OH15', 'zanesville / cambridge', 'zanesville']
                        ]),
        State(38, 'Oklahoma', 'OK', 'oklahoma', [
                        ['OK01', 'lawton', 'lawton'],
                        ['OK02', 'northwest OK', 'enid'],
                        ['OK03', 'oklahoma city', 'oklahomacity'],
                        ['OK04', 'stillwater', 'stillwater'],
                        ['OK05', 'tulsa', 'tulsa']
                        ]),
        State(39, 'Oregon', 'OR', 'oregon', [
                        ['OR01', 'bend', 'bend'],
                        ['OR02', 'Corvallis / Albany', 'corvallis'],
                        ['OR03', 'east oregon', 'eastoregon'],
                        ['OR04', 'eugene', 'eugene'],
                        ['OR05', 'klamath falls', 'klamath'],
                        ['OR06', 'medford-ashland', 'medford'],
                        ['OR07', 'oregon coast', 'oregoncoast'],
                        ['OR08', 'portland', 'portland'],
                        ['OR09', 'roseburg', 'roseburg'],
                        ['OR10', 'salem', 'salem']
                        ]),
        State(40, 'Pennsylvania', 'PA', 'pennsylvania', [
                        ['PA01', 'Altoona-Johnstown', 'altoona'],
                        ['PA02', 'cumberland valley', 'chambersburg'],
                        ['PA03', 'erie', 'erie'],
                        ['PA04', 'harrisburg', 'harrisburg'],
                        ['PA05', 'lancaster', 'lancaster'],
                        ['PA06', 'lehigh valley', 'allentown'],
                        ['PA07', 'meadville', 'meadville'],
                        ['PA08', 'philadelphia', 'philadelphia'],
                        ['PA09', 'pittsburgh', 'pittsburgh'],
                        ['PA10', 'poconos', 'poconos'],
                        ['PA11', 'reading', 'reading'],
                        ['PA12', 'scranton / wilkes-barre', 'scranton'],
                        ['PA13', 'state college', 'pennstate'],
                        ['PA14', 'williamsport', 'williamsport'],
                        ['PA15', 'york', 'york']
                        ]),
        State(41, 'Rhode Island', 'RI', 'rhodeisland', [
                        ['RI01', 'Providence', 'providence']
                        ]),
        State(42, 'South Carolina', 'SC', 'southcarolina', [
                        ['SC01', 'charleston', 'charleston'],
                        ['SC02', 'columbia', 'columbia'],
                        ['SC03', 'florence', 'florencesc'],
                        ['SC04', 'greenville / upstate', 'greenville'],
                        ['SC05', 'hilton head', 'hiltonhead'],
                        ['SC06', 'myrtle beach', 'myrtlebeach']
                        ]),
        State(43, 'South Dakota', 'SD', 'southdakota', [
                        ['SD01', 'northeast SD', 'nesd'],
                        ['SD02', 'pierre / central SD', 'csd'],
                        ['SD03', 'rapid city / west SD', 'rapidcity'],
                        ['SD04', 'sioux falls / SE SD', 'siouxfalls'],
                        ['SD05', 'south dakota', 'sd']
                        ]),
        State(44, 'Tennessee', 'TN', 'tennessee', [
                        ['TN01', 'chattanooga', 'chattanooga'],
                        ['TN02', 'clarksville', 'clarksville'],
                        ['TN03', 'cookeville', 'cookeville'],
                        ['TN04', 'jackson', 'jacksontn'],
                        ['TN05', 'knoxville', 'knoxville'],
                        ['TN06', 'memphis', 'memphis'],
                        ['TN07', 'nashville', 'nashville'],
                        ['TN08', 'tri-cities', 'tricities']
                        ]),
        State(45, 'Texas', 'TX', 'texas', [
                        ['TX01', 'abilene', 'abilene'],
                        ['TX02', 'amarillo', 'amarillo'],
                        ['TX03', 'austin', 'austin'],
                        ['TX04', 'beaumont / port arthur', 'beaumont'],
                        ['TX05', 'brownsville', 'brownsville'],
                        ['TX06', 'college station', 'collegestation'],
                        ['TX07', 'corpus christi', 'corpuschristi'],
                        ['TX08', 'dallas / fort worth', 'dallas'],
                        ['TX09', 'deep east texas', 'nacogdoches'],
                        ['TX10', 'del rio / eagle pass', 'delrio'],
                        ['TX11', 'el paso', 'elpaso'],
                        ['TX12', 'galveston', 'galveston'],
                        ['TX13', 'houston', 'houston'],
                        ['TX14', 'killeen / temple / ft hood', 'killeen'],
                        ['TX15', 'laredo', 'laredo'],
                        ['TX16', 'lubbock', 'lubbock'],
                        ['TX17', 'mcallen / edinburg', 'mcallen'],
                        ['TX18', 'odessa / midland', 'odessa'],
                        ['TX19', 'san angelo', 'sanangelo'],
                        ['TX20', 'san antonio', 'sanantonio'],
                        ['TX21', 'san marcos', 'sanmarcos'],
                        ['TX22', 'southwest TX', 'bigbend'],
                        ['TX23', 'texoma', 'texoma'],
                        ['TX24', 'tyler / east TX', 'easttexas'],
                        ['TX25', 'victoria', 'victoriatx'],
                        ['TX26', 'waco', 'waco'],
                        ['TX27', 'wichita falls', 'wichitafalls']
                        ]),
        State(46, 'Utah', 'UT', 'utah', [
                        ['UT01', 'logan', 'logan'],
                        ['UT02', 'ogden-clearfield', 'ogden'],
                        ['UT03', 'provo / orem', 'provo'],
                        ['UT04', 'salt lake city', 'saltlakecity'],
                        ['UT05', 'st george', 'stgeorge']
                        ]),
        State(47, 'Vermont', 'VT', 'vermont', [
                        ['VT01', 'burlington', 'burlington']
                        ]),
        State(48, 'Virginia', 'VA', 'virginia', [
                        ['VA01', 'charlottesville', 'charlottesville'],
                        ['VA02', 'danville', 'danville'],
                        ['VA03', 'fredericksburg', 'fredericksburg'],
                        ['VA04', 'hampton roads', 'norfolk'],
                        ['VA05', 'harrisonburg', 'harrisonburg'],
                        ['VA06', 'lynchburg', 'lynchburg'],
                        ['VA07', 'new river valley', 'blacksburg'],
                        ['VA08', 'richmond', 'richmond'],
                        ['VA09', 'roanoke', 'roanoke'],
                        ['VA10', 'southwest VA', 'swva'],
                        ['VA11', 'winchester', 'winchester']
                        ]),
        State(49, 'Washington', 'WA', 'washington', [
                        ['WA01', 'bellingham', 'bellingham'],
                        ['WA02', 'Kennewick-Pasco-Richland', 'kpr'],
                        ['WA03', 'moses lake', 'moseslake'],
                        ['WA04', 'olympic peninsula', 'olympic'],
                        ['WA05', 'pullman / moscow', 'pullman'],
                        ['WA06', 'Seattle-Tacoma', 'seattle'],
                        ['WA07', 'skagit / island / SJI', 'skagit'],
                        ['WA08', 'spokane / coeur d\'alene', 'spokanewenatchee'],
                        ['WA09', 'wenatchee', 'wenatchee'],
                        ['WA10', 'yakima', 'yakima']
                        ]),
        State(50, 'West Virginia', 'WV', 'westvirginia', [
                        ['WV01', 'charleston', 'charlestonwv'],
                        ['WV02', 'eastern panhandle', 'martinsburg'],
                        ['WV03', 'Huntington-Ashland', 'huntington'],
                        ['WV04', 'morgantown', 'morgantown'],
                        ['WV05', 'northern panhandle', 'wheeling'],
                        ['WV06', 'Parkersburg-Marietta', 'parkersburg'],
                        ['WV07', 'southern WV', 'swv'],
                        ['WV08', 'west virginia (old)', 'wv']
                        ]),
        State(51, 'Wisconsin', 'WI', 'wisconsin', [
                        ['WI01', 'Appleton-Oshkosh-FDL', 'appleton'],
                        ['WI02', 'eau claire', 'eauclaire'],
                        ['WI03', 'green bay', 'greenbay'],
                        ['WI04', 'janesville', 'janesville'],
                        ['WI05', 'Kenosha-Racine', 'racine'],
                        ['WI06', 'la crosse', 'lacrosse'],
                        ['WI07', 'madison', 'madison'],
                        ['WI08', 'milwaukee', 'milwaukee'],
                        ['WI09', 'northern WI', 'northernwi'],
                        ['WI10', 'sheboygan', 'sheboygan'],
                        ['WI11', 'wausau', 'wausau']
                        ]),
        State(52, 'Wyoming', 'WY', 'wyoming', [
                        ['WY01', 'casper', 'casper'],
                        ['WY02', 'cheyenne', 'cheyenne']
                        ]),
        State(53, 'Territories', 'US', 'territories', [
                        ['US01', 'Guam-Micronesia', 'micronesia'],
                        ['US02', 'puerto rico', 'puertorico'],
                        ['US03', 'U.S. virgin islands', 'virgin']
                        ]),
]