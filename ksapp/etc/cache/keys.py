# -*- coding: utf-8 -*-

class CacheKeys(object):

    def __init__(self):
        pass

    # No query results cache string
    NO_QUERY_RESULTS = u'no results'

    # listings
    USERS_SIDEBAR_SHELF_LISTING = u'user_%s_sidebar_shelf_listing' # % user_username
    USERS_SIDEBAR_BOOK_LISTING = u'user_%s_sidebar_book_listing' # % user_username
    USERS_FULL_SHELF_LISTING_SORT_ALPHA = u'user_%s_full_shelf_listing' # % user_username
    USERS_FULL_BOOK_LISTING_SORT_ALPHA = u'user_%s_full_book_listing' # % user_username
    USERS_LISTING_OF_BOOKS_ON_SHELF_SORT_ALPHA = u'user_%s_shelf_%s_book_listing_full_sorted_alpha' # % user_username, shelf_slug
    USERS_LISTING_OF_JOTS_IN_BOOK_SORT_NEWEST_FIRST = u'user_%s_listing_of_jots_in_shelf_%s_book_%s_sorted_newest_first' # % user_username, shelf_slug, book_slug

    # single row
    SINGLE_SHELF_DETAILS_FROM_SLUG = u'single_shelf_details_user_%s_slug_%s' # user_username, slug
    SINGLE_BOOK_DETAILS_FROM_SLUG = u'single_book_details_user_%s_shelf_slug_%s_book_slug_%s' # user_username, shelf_slug, book_slug
    SINGLE_JOT_DETAILS_FROM_ID = u'single_jot_details_id_%s' # jot_id

    

    ### redis keys

    GLOBAL_ACTIVITY_STREAM_LIST_OF_ACTIVITY_IDS = u'activity_stream:global:activity_id_list'
    GLOBAL_ACTIVITY_STREAM_NEXT_ID = u'activity_stream:global:next_id'
    ACTIVITY_DETAILS = u'activity_stream:details:%s' # % activity_id
    LISTING_OF_SHELFS_BOOK_SLUGS_SORT_ALPHA = u'listing:user_%s:shelf_%s:book_slugs:sorted_alpha' # % username, shelf_slug


