# -*- coding: utf-8 -*-

import os
import urlparse
import bmemcached
import json



# (Heroku) Connect to memcache with config from environment variables
if os.environ.get('PROD'):
  cache = bmemcached.Client(os.environ.get('MEMCACHEDCLOUD_SERVERS').split(','), os.environ.get('MEMCACHEDCLOUD_USERNAME'), os.environ.get('MEMCACHEDCLOUD_PASSWORD'))

else:
    # Connect to local memcache.
    cache = bmemcached.Client(servers=['127.0.0.1'])