# -*- coding: utf-8 -*-

from ksapp.etc.cache.setup import cache
from ksapp.etc.cache.keys import CacheKeys

cachekey = CacheKeys()


class CacheUtils(object):

    def __init__(self):
        pass

    
    def check_cache_for_no_query_results(self, v):
        if v == cachekey.NO_QUERY_RESULTS:
            return False
        else:
            return v


    def cache_set_results_or_no_results(self, key, results):
        if results:
            cache.set(key, results)
        else:
            cache.set(key, cachekey.NO_QUERY_RESULTS)
        return


