# # -*- coding: utf-8 -*-

from ksapp.mods.shelf.utils import ShelfUtils
from ksapp.mods.book.utils import BookUtils
from ksapp.mods.jot.utils import JotUtils


shelf_util = ShelfUtils()
book_util = BookUtils()
jot_util = JotUtils()



class CacheUpdates(object):

    def __init__(self):
        pass


    # shelf updates
    
    def user_creates_new_shelf(self, username, shelf_details):
        username = shelf_details.creator_username
        shelf_slug = shelf_details.actual_slug

        # update shelf details
        shelf_util.cache_update_shelf_details_by_shelf_details(shelf_details)

        # update users full shelf listing
        shelf_util.cache_update_shelf_listing_full_by_username(username)

        # update users sidebar shelf listing
        shelf_util.cache_update_shelf_listing_sidebar_by_username(username)

        return


    def shelf_name_updated(self,
                           original_shelf_details,
                           shelf_details,
                           actual_slug):
        username = shelf_details.creator_username
        original_shelf_slug = original_shelf_details.actual_slug

        # delete the cache for the original shelf name
        shelf_util.cache_delete_shelf_details_by_shelf_details(original_shelf_details)
        shelf_util.cache_update_shelf_listing_sidebar_by_username(shelf_details.creator_username)
        shelf_util.cache_update_shelf_listing_full_by_username(shelf_details.creator_username)

        book_util.cache_update_shelfs_book_listing_by_shelf_details(original_shelf_details)
        book_util.cache_update_shelfs_book_listing_by_shelf_details(shelf_details)
        book_util.cache_update_book_listing_sidebar_by_username(shelf_details.creator_username)
        book_util.cache_update_book_listing_full_by_username(shelf_details.creator_username)
        # delete redis sorted set for this shelves prev/next book slug
        book_util.delete_prev_and_next_book_in_shelf_slugs_sorted_set(username, original_shelf_slug)

        return


    def shelf_deleted_from_db(self, shelf_details):
        username = shelf_details.creator_username
        shelf_slug = shelf_details.actual_slug

        # remove shelf details
        shelf_util.cache_delete_shelf_details_by_shelf_details(shelf_details)

        # remove shelfs book listing
        shelf_util.cache_delete_shelfs_book_listing_by_username_and_shelf_slug(username, shelf_slug)

        # update users full shelf listing
        shelf_util.cache_update_shelf_listing_full_by_username(username)

        # update users sidebar shelf listing
        shelf_util.cache_update_shelf_listing_sidebar_by_username(username)

        # update users full & sidebar book listing
        book_util.cache_update_book_listing_sidebar_by_username(shelf_details.creator_username)
        book_util.cache_update_book_listing_full_by_username(shelf_details.creator_username)

        # delete redis sorted set for this shelves prev/next book slug
        book_util.delete_prev_and_next_book_in_shelf_slugs_sorted_set(username, shelf_slug)

        return


    def shelf_attributes_updated(self, shelf_details):
        username = shelf_details.creator_username
        shelf_slug = shelf_details.actual_slug

        # update shelf details
        shelf_util.cache_update_shelf_details_by_shelf_details(shelf_details)

        # update users full shelf listing
        shelf_util.cache_update_shelf_listing_full_by_username(username)

        # update users sidebar shelf listing
        shelf_util.cache_update_shelf_listing_sidebar_by_username(username)

        return


    def shelf_stat_updated(self, shelf_details):
        username = shelf_details.creator_username
        shelf_slug = shelf_details.actual_slug

        # update shelf details
        shelf_util.cache_update_shelf_details_by_shelf_details(shelf_details)

        # update users full shelf listing
        shelf_util.cache_update_shelf_listing_full_by_username(username)

        return







    

    # book updates

    def user_creates_new_book(self, username, book_details):
        username = book_details.creator_username
        shelf_slug = book_details.shelf_slug

        book_util.cache_update_shelfs_book_listing_by_book_details(book_details)
        book_util.cache_update_book_listing_sidebar_by_username(book_details.creator_username)
        book_util.cache_update_book_listing_full_by_username(username)
        book_util.cache_update_book_details_by_book_details(book_details)
        # updated prev/next book in shelf sorted set
        book_util.update_prev_and_next_book_in_shelf_slugs_sorted_set(username, shelf_slug)

        return


    def book_last_modified_update_with_jot_details(self, jot_details):
        book_util.edit_book_details_last_modified_to_right_now(jot_details.creator_username,
                                                         jot_details.shelf_slug,
                                                         jot_details.book_slug)
        book_util.cache_update_book_listing_sidebar_by_username(jot_details.creator_username)

        return


    def book_name_updated(self,
                          original_book_details,
                          book_details,
                          actual_slug):
        username = book_details.creator_username
        shelf_slug = book_details.shelf_slug

        # shelf_util.cache_update_shelf_listing_sidebar_by_username(book_details.creator_username)
        # shelf_util.cache_update_shelf_listing_full_by_username(book_details.creator_username)

        book_util.cache_update_shelfs_book_listing_by_book_details(book_details)
        book_util.cache_update_book_listing_sidebar_by_username(book_details.creator_username)
        book_util.cache_update_book_listing_full_by_username(book_details.creator_username)
        book_util.cache_update_book_details_by_book_details(book_details)
        # updated prev/next book in shelf sorted set
        book_util.update_prev_and_next_book_in_shelf_slugs_sorted_set(username, shelf_slug)

        return


    def book_deleted_from_db(self, book_details, multi):
        username = book_details.creator_username
        shelf_slug = book_details.shelf_slug

        # delete cache'd listing of book's jots
        jot_util.cache_delete_jot_listing_by_book_slug(book_details.creator_username,
                                                       book_details.shelf_slug,
                                                       book_details.actual_slug)
        # delete book details
        book_util.cache_delete_book_details_by_details(book_details)

        # if not multi (from shelf deletion), then update all the listings
        if multi == None:
            # update shelfs book listing
            book_util.cache_update_shelfs_book_listing_by_book_details(book_details)
            # update users book listings
            book_util.cache_update_book_listing_sidebar_by_username(book_details.creator_username)
            book_util.cache_update_book_listing_full_by_username(book_details.creator_username)

        # updated prev/next book in shelf sorted set
        book_util.update_prev_and_next_book_in_shelf_slugs_sorted_set(username, shelf_slug)
        

        return


    def book_shelf_modified(self, original_book_details, new_book_details):
        username = new_book_details.creator_username
        original_shelf_slug = original_book_details.shelf_slug
        new_shelf_slug = new_book_details.shelf_slug

        # delete caches no longer relevant
        book_util.cache_delete_book_details_by_details(original_book_details)
        jot_util.cache_delete_jot_listing_by_book_slug(new_book_details.creator_username,
                                                       new_book_details.shelf_slug,
                                                       new_book_details.actual_slug)
        # update cache's that changed
        book_util.cache_update_shelfs_book_listing_by_book_details(original_book_details)
        book_util.cache_update_shelfs_book_listing_by_book_details(new_book_details)
        book_util.cache_update_book_listing_sidebar_by_username(new_book_details.creator_username)
        book_util.cache_update_book_listing_full_by_username(new_book_details.creator_username)
        book_util.cache_update_book_details_by_book_details(new_book_details)

        # updated prev/next book in shelf sorted set for original shelf and new shelf
        book_util.update_prev_and_next_book_in_shelf_slugs_sorted_set(username, original_shelf_slug)
        book_util.update_prev_and_next_book_in_shelf_slugs_sorted_set(username, new_shelf_slug)

        return


    def book_attributes_updated(self, book_details):
        username = book_details.creator_username
        shelf_slug = book_details.shelf_slug
        book_slug = book_details.actual_slug

        # update book details
        book_util.cache_update_book_details_by_book_details(book_details)

        # update users full book listing
        book_util.cache_update_book_listing_full_by_username(username)

        # update users sidebar book listing
        book_util.cache_update_book_listing_sidebar_by_username(username)

        # update shelf's book listing
        book_util.cache_update_shelfs_book_listing_by_book_details(book_details)

        return


    def book_stat_updated(self, book_details):
        username = book_details.creator_username
        shelf_slug = book_details.shelf_slug
        book_slug = book_details.actual_slug

        # update book details
        book_util.cache_update_book_details_by_book_details(book_details)

        # update users full book listing
        book_util.cache_update_book_listing_full_by_username(username)

        return










    # jot updates


    def user_creates_new_jot(self, jot_details):
        self.book_last_modified_update_with_jot_details(jot_details)
        jot_util.cache_update_jot_listing_by_book_slug(jot_details.creator_username,
                                                 jot_details.shelf_slug,
                                                 jot_details.book_slug)

        return


    def jot_deleted_from_db(self, jot_details, book_modified_update=None):
        jot_util.cache_update_jot_listing_by_book_slug(jot_details.creator_username,
                                                 jot_details.shelf_slug,
                                                 jot_details.book_slug)
        if book_modified_update:
            self.book_last_modified_update_with_jot_details(jot_details)

        return


    def jot_edited(self, jot_details):
        jot_util.cache_update_jot_details(jot_details)
        jot_util.cache_update_jot_listing_by_book_slug(jot_details.creator_username,
                                                 jot_details.shelf_slug,
                                                 jot_details.book_slug)
        self.book_last_modified_update_with_jot_details(jot_details)

        return


    def jots_shelf_updated(self, original_book_details, new_book_details):
        jot_util.cache_delete_jot_listing_by_book_slug(original_book_details.creator_username,
                                                       original_book_details.shelf_slug,
                                                       original_book_details.actual_slug)
        jot_util.cache_update_jot_listing_by_book_slug(new_book_details.creator_username,
                                                       new_book_details.shelf_slug,
                                                       new_book_details.actual_slug)
        return


    
