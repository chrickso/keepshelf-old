# -*- coding: utf-8 -*-

# # This mod can not be imported by functions or utils # #

from ksapp.extensions import db

from ksapp.mods.shelf.utils import ShelfUtils
from ksapp.mods.book.utils import BookUtils
from ksapp.mods.jot.utils import JotUtils

from ksapp.mods.shelf.functions import ShelfFunctions
from ksapp.mods.book.functions import BookFunctions
from ksapp.mods.jot.functions import JotFunctions

from ksapp.etc.messages import Messages

from ksapp.etc.cache.updates import CacheUpdates



shelf_util = ShelfUtils()
book_util = BookUtils()
jot_util = JotUtils()
shelf_func = ShelfFunctions()
book_func = BookFunctions()
jot_func = JotFunctions()
messages = Messages()
cache_update = CacheUpdates()


class RenameOperations(object):

    def __init__(self):
        pass


    def modify_shelf_name(self,
                          original_shelf_details,
                          new_shelf_name,
                          base_slug,
                          actual_slug):
        # get the original shelf details
        shelf_details = shelf_util.lookup_shelf_details_by_id(original_shelf_details.id)

        # if shelf_details:
        # modify the record to the updated data
        shelf_details.name = new_shelf_name
        shelf_details.base_slug = base_slug
        shelf_details.actual_slug = actual_slug

        db.session.commit()

        # update all jot records with the new shelf name & slug
        jot_func.update_all_jots_with_old_shelf_name_and_slug_with_new_shelf_name_and_slug(original_shelf_details,
                                                                                           shelf_details)

        # update all the book records with the new shelf name & slug
        # books come second so to effeciently update the books jot listing
        self.update_all_books_with_old_shelf_name_and_slug_with_new_name_and_slug(original_shelf_details,
                                                                                       shelf_details)

        # delete the old shelf's cache'd details
        shelf_util.cache_delete_shelf_details_by_username_and_slug(original_shelf_details.creator_username, original_shelf_details.actual_slug)

        # regenerate all effected cached queries
        cache_update.shelf_name_updated(original_shelf_details, shelf_details, actual_slug)

        return shelf_details

        # else:
        #     return False


    def update_all_books_with_old_shelf_name_and_slug_with_new_name_and_slug(   self,
                                                                                original_shelf_details,
                                                                                new_shelf_details
                                                                                                        ):
        # get a list of all books in the shelf that was updated
        all_records = book_util.lookup_book_listing_full_by_username_and_shelf_slug(original_shelf_details.creator_username,
                                                                          original_shelf_details.actual_slug)

        # for every record found
        for record in all_records:

            # delete the cached details (they are now outdated)
            book_util.cache_delete_book_details_by_details(record)

            # update the database with new information
            record.shelf_slug = new_shelf_details.actual_slug
            record.shelf_name = new_shelf_details.name

            # add the updated record to the session
            db.session.add(record)

            # set the new book details in cache
            book_util.cache_update_book_details_by_book_details(record)

            # update the books jot listing
            jot_util.cache_update_jot_listing_by_book_slug(record.creator_username,
                                                           record.shelf_slug,
                                                           record.actual_slug)

        # commit all changes to updated records
        db.session.commit()

        return True


    def modify_book_name(self,
                         original_book_details,
                         new_book_name,
                         base_slug,
                         actual_slug):

        creator_username = original_book_details.creator_username
        shelf_slug = original_book_details.shelf_slug
        book_slug = original_book_details.actual_slug

        # get the original book details
        book_details = book_util.lookup_book_details_by_username_and_shelf_slug_and_book_slug(creator_username, shelf_slug, book_slug)

        # modify the record to the updated data
        book_details.name = new_book_name
        book_details.base_slug = base_slug
        book_details.actual_slug = actual_slug

        db.session.commit()

        # update all jot records with the new book name & slug
        jot_func.update_all_jots_with_old_book_name_and_slug_with_new_book_name_and_slug(original_book_details,
                                                                                         book_details)

        # delete old book name cache'd details
        book_util.cache_delete_book_details_by_details(original_book_details)

        # regenerate all effected cached queries
        cache_update.book_name_updated(original_book_details, book_details, actual_slug)


        return book_details




class DeleteOperations(object):

    def __init__(self):
        pass


    def delete_shelf_and_books_and_jots_by_shelf_details(self, shelf_details):

        username = shelf_details.creator_username
        shelf_slug = shelf_details.actual_slug

        # get shelf details and delete from ShelfDB
        original_shelf_details = shelf_util.lookup_shelf_details_by_id(shelf_details.id)
        db.session.delete(original_shelf_details)
        db.session.commit()

        # lookup shelf's book listing and delete each of them and their jots
        shelfs_book_list = book_util.get_shelfs_book_listing_by_username_and_shelf_slug_sorted_alpha(username, shelf_slug)

        if shelfs_book_list:
            for book in shelfs_book_list:
                book_details = book_util.get_book_details_by_username_and_shelf_slug_and_book_slug(username, shelf_slug, book.actual_slug)
                self.delete_book_and_jots_by_book_details(book_details, True)

        # update all cache's referencing the deleted shelf
        cache_update.shelf_deleted_from_db(original_shelf_details)

        return True


    def delete_book_and_jots_by_book_details(self, book_details, multi=None):
        username = book_details.creator_username
        shelf_slug = book_details.shelf_slug

        # get book details and delete from BookDB
        original_book_details = book_util.lookup_book_details_by_id(book_details.id)
        db.session.delete(original_book_details)
        db.session.commit()

        # if this is not a shelf delete, subtract one from the shelf's book_count
        if multi == None:
            shelf_details = shelf_func.edit_shelf_details_subtract_1_from_book_count_and_update_caches(username, shelf_slug)

        # get all jot's associtated with deleted book
        books_jot_list = jot_util.get_jot_listing_by_book_slug(book_details.creator_username, book_details.shelf_slug, book_details.actual_slug)

        # if book had jots, cycle thru delete'd book's jot and delete them
        # and their cache'd details
        if books_jot_list:
            for jot_details in books_jot_list:
                jot_util.delete_jot_from_db(jot_details.id)
                jot_util.cache_delete_jot_details_by_id(jot_details.id)

        # update all cache's referencing the deleted book
        cache_update.book_deleted_from_db(book_details, multi)

        return True
