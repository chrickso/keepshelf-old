# -*- coding: utf-8 -*-

class Messages(object):

	# success



	# notices




	# errors
	SHELF_INVALID_SLUG 	= u'Your shelf name must contain at least one A-z, 0-9 character.'
	BOOK_INVALID_SLUG 	= u'Your shelf name must contain at least one A-z, 0-9 character.'

	SHELF_NO_NAME_PROVIDED = u'You must provide a shelf name.'

	SHELF_NOT_NEW = u'The shelf you selected is not different than the current shelf.'