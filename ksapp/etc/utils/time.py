# -*- coding: utf-8 -*-

## UTILS CANNOT IMPORT OTHER UTILS

from datetime import datetime, timedelta

class TimeUtils(object):

    def get_current_time(self):
        return datetime.utcnow()


    def pretty_date(self, dt, default=None):
        """
        Returns string representing "time since" e.g.
        3 days ago, 5 hours ago etc.
        Ref: https://bitbucket.org/danjac/newsmeme/src/a281babb9ca3/newsmeme/
        """

        if default is None:
            default = 'just now'

        now = datetime.utcnow()
        diff = now - dt

        periods = (
            (diff.days / 365, 'year', 'years'),
            (diff.days / 30, 'month', 'months'),
            (diff.days / 7, 'week', 'weeks'),
            (diff.days, 'day', 'days'),
            (diff.seconds / 3600, 'hour', 'hours'),
            (diff.seconds / 60, 'min', 'mins'),
            (diff.seconds, 'sec', 'secs'),
        )

        for period, singular, plural in periods:

            if not period:
                continue

            if period == 1:
                return u'%d %s ago' % (period, singular)
            else:
                return u'%d %s ago' % (period, plural)

        return default