# -*- coding: utf-8 -*-

## UTILS CANNOT IMPORT OTHER UTILS

from ksapp.extensions import db

from ksapp.etc.models.user_model import UserDB
# from pbapp.mods.post import PostDB


def create_default_users():
    new_user = UserDB(
                    username = 'chrickso',
                    password = 'Dratini11',
                    creator_ip = '127.0.0.1',
                    settings={'default_share_level': 'public'}
                  )
    db.session.add(new_user)

    new_user = UserDB(
                    username = 'chrinon',
                    password = 'Dratini11',
                    creator_ip = '127.0.0.1',
                    settings={'default_share_level': 'private'}
                  )
    db.session.add(new_user)

    new_user = UserDB(
                    username = 'delete',
                    password = 'Dratini11',
                    creator_ip = '127.0.0.1',
                    settings={'default_share_level': 'public'}
                  )
    db.session.add(new_user)

    return db.session