# -*- coding: utf-8 -*-

## UTILS CANNOT IMPORT OTHER UTILS

import string
import random
import re

from jinja2 import Markup, escape
from jinja2.utils import urlize

class StringFunctions(object):

    # random string generator
    # http://stackoverflow.com/questions/2257441/python-random-string-generation-with-upper-case-letters-and-digits
    def random_string(self, size=9, chars=string.ascii_lowercase + string.digits):
        return ''.join(random.choice(chars) for x in range(size))

    # remove all whitespace & newlines
    # http://stackoverflow.com/questions/3984539/python-use-regular-expression-to-remove-the-white-space-from-all-lines
    def no_whitespace(self, s):
        """Remove all whitespace & newlines. """
        s = ''.join(re.sub(r"(?m)\s+", "", s))
        return s


    # parse tags
    # http://stackoverflow.com/a/12082914/523051
    #
    def parseTags(self, str):
        return remove_duplicates(filter(None, map(sanitizeTag, str.split(','))))

    def sanitizeTag(self, str):
        words    = filter(lambda c: c.isalnum() or c.isspace(), str).split()
        numWords = len(words)
        if numWords == 0:
            return None
        elif numWords == 1:
            return words[0]
        else:
            words0 = words[0].lower() if words[0][0].islower() else words[0].capitalize()
            return words0 + ''.join(w.capitalize() for w in words[1:])

    def remove_duplicates(self, seq, idfun=None):
        # f5 from here http://www.peterbe.com/plog/uniqifiers-benchmark
        # order preserving
        if idfun is None:
            def idfun(x): return x
        seen = {}
        result = []
        for item in seq:
            marker = idfun(item.lower())

            if marker in seen: continue
            seen[marker] = 1
            result.append(item)
        return result


    # remove all double quotes & single quotes from beginning & end of string
    def removeQuotes(self, str):
        """Removes single/double quotes and spaces from ends of a str"""
        s = str.strip(' "\'\t\r\n')
        return s


    def removeEndSpace(self, str):
        """Removes all spaces from ends of a strings"""
        if str:
            s = str.strip(' \t\r\n')
            return s
        else:
            return None



    # # same as jinja filter in app.py
    def nl2br(self, value):
        _paragraph_re = re.compile(r'(?:\r\n|\r(?!\n)|\n){2,}')
        result = u'\n\n'.join(u'<p>%s</p>' % p.replace('\r\n', Markup('<br>\n')) for p in _paragraph_re.split(escape(value)))
        result = Markup(result)
        return result

    def safe_urlize(self, value):
        esc_val = escape(value)
        urlized_val = urlize(esc_val, nofollow=True)
        return Markup(urlized_val)


    def sanitize_username(self, s):
        """Takes a string and returns it with only allowed characters."""
        # http://stackoverflow.com/questions/12756156/with-python-what-is-the-most-efficient-way-to-remove-all-char-from-string-excep#comment17236154_12756156
        # a-z, A-Z, 0-9, _, -, .
        keep_set = set(string.ascii_letters + string.digits + '_-.')
        return filter(keep_set.__contains__, s)


    def sanitize_board_url(self, s):
        """Takes a string and returns it with only allowed characters."""
        # http://stackoverflow.com/questions/12756156/with-python-what-is-the-most-efficient-way-to-remove-all-char-from-string-excep#comment17236154_12756156
        # a-z, A-Z, 0-9, _, -, .
        keep_set = set(string.ascii_letters + string.digits + '_-')
        return filter(keep_set.__contains__, s)


    def clean_board_category_name(self, s):
        """Takes a string and returns it with only allowed characters."""
        # http://stackoverflow.com/questions/12756156/with-python-what-is-the-most-efficient-way-to-remove-all-char-from-string-excep#comment17236154_12756156
        # a-z, A-Z,
        keep_set = set(string.ascii_letters)
        return filter(keep_set.__contains__, s)


    def sanitize_board_name(self, s):
        """Takes a string and returns it with only allowed characters."""
        # http://stackoverflow.com/questions/12756156/with-python-what-is-the-most-efficient-way-to-remove-all-char-from-string-excep#comment17236154_12756156
        # a-z, A-Z, 0-9, _, -, .
        keep_set = set(string.ascii_letters + string.digits + '_-.')
        return filter(keep_set.__contains__, s)


