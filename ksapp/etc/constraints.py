# -*- coding: utf-8 -*-

class Constraints(object):

	# Books

	BOOK_NAME_MAX_CHARS = 500


	# Shelves

	SHELF_NAME_MAX_CHARS = 500


	# Sidebar
	# NUMBER_OF_RECENT_SHELVES_TO_LIST = 10
	# NUMBER_OF_RECENT_BOOKS_TO_LIST = 10


	# Slugs

	SLUGS_MAX_SLUG_CHARS = 500


	# Users

	USERS_MAX_EMAIL_CHARS = 255
	USERS_USERNAME_MAX_CHARS = 40


