# -*- coding: utf-8 -*-

from flask import flash, redirect, request, session, url_for

from ksapp.extensions import db
from ksapp.etc.models.shelf_model import ShelfDB

from ksapp.etc.cache.updates import CacheUpdates
from ksapp.etc.activity.updates import ActivityUpdates

from ksapp.mods.shelf.functions import ShelfFunctions
from ksapp.mods.book.functions import BookFunctions

from ksapp.mods.shelf.utils import ShelfUtils
from ksapp.mods.book.utils import BookUtils
from ksapp.mods.jot.utils import JotUtils

from ksapp.mods.jot.functions import JotFunctions
from ksapp.etc.utils.strings import StringFunctions

from ksapp.etc.constraints import Constraints
from ksapp.etc.messages import Messages

from ksapp.etc.utils.slug import slugify

from ksapp.etc.operations import RenameOperations

# # from pbapp.mods.etc.cache.setup import cache
# from pbapp.mods.etc.cache.keys import CacheKeys

# from pbapp.mods.etc.utils.post_utils import PostUtils

# from pbapp.extensions import redis


# cachekey = CacheKeys()
# post_util = PostUtils()

cache_update = CacheUpdates()
activity_update = ActivityUpdates()

shelf_func = ShelfFunctions()
book_func = BookFunctions()

shelf_util = ShelfUtils()
book_util = BookUtils()
jot_util = JotUtils()

jot_func = JotFunctions()
string_func = StringFunctions()

constraints = Constraints()
messages = Messages()

rename_op = RenameOperations()


class ShelfForms(object):

    def __init__(self):
        pass


    def process_form_shelf_new(self, shelf_name=None):

        # set empty dictionaries in case needed
        post_inputs={}
        post_errors={}

        # add attributes dict to post_inputs
        post_inputs['attributes'] = {}


        # set empty success_url to return if not triggered
        success_url = None


        # get user_username from session
        user_username = session['user_username']


        # set variables to be sent back if the logic fails to provide
        new_shelf_details = None


        # if shelf_name was supplied, keep it, otherwise get it from form and clean
        if not shelf_name:
            shelf_name = string_func.removeEndSpace(request.form.get('shelf_name'))


        # populate dictionaries with form fields or errors if required
        if shelf_name:
            post_inputs['shelf_name'] = shelf_name
        else:
            post_errors['shelf_name'] = messages.SHELF_NO_NAME_PROVIDED

        if 'pin_to_sidebar' in request.form:
            pin_state = '1'
            post_inputs['attributes']['pin'] = '1'
        else:
            pin_state = '0'
            post_inputs['attributes']['pin'] = '0'

        visibility = request.form.get('visibility')

        if visibility.lower() == 'private':
            visibility = 'private'
            post_inputs['attributes']['visibility'] = 'private'
        else:
            visibility = 'public'
            post_inputs['attributes']['visibility'] = 'public'


        # if a shelf name was provided, slugify it and confirm it does not contain all removed chars
        if shelf_name:
            base_slug = slugify(shelf_name)

            if base_slug == None:
                post_errors['shelf_name'] = messages.SHELF_INVALID_SLUG


        ## validate the form submissions meets standard criteria


        # do inputs fall within character length range?
        if shelf_name and len(shelf_name) > constraints.SHELF_NAME_MAX_CHARS:
            post_errors['shelf_name'] = u'That name is too long. Max: %s characters.' % constraints.SHELF_NAME_MAX_CHARS


        # if no errors, generate shelf name slug
        if not post_errors.get('shelf_name'):

            actual_slug = base_slug

            duplicate_slug_count = shelf_util.count_shelves_matching_username_and_slug(user_username, base_slug)

            if duplicate_slug_count > 1:
                actual_slug = base_slug + '_%s' % duplicate_slug_count
                shelf_name = shelf_name + ' (%s)' % duplicate_slug_count

            # if everything passes, insert new shelfk in ShelfDB

            new_shelf_details = shelf_func.db_insert_new_shelf_plus_updates(
                                                                        shelf_name,
                                                                        base_slug,
                                                                        actual_slug,
                                                                        pin_state,
                                                                        visibility 
                                                                                    )

            if not new_shelf_details:
                flash(u'Something went wrong while creating your shelf', u'error')
            else:
                success_url = url_for('shelfBP.details',
                                       user_username=session['user_username'],
                                       shelf_slug=new_shelf_details.actual_slug)

        return post_inputs, post_errors, success_url, new_shelf_details    



    def process_form_shelf_listing_pin_and_unpin(self):

        do_pin_ids = request.form.getlist("do_pin")
        do_unpin_ids = []

        do_unpin_ids_submitted = request.form.getlist("do_unpin")
        pind_list_ids = request.form.getlist("pind_list")

        for entry in pind_list_ids:
            if entry not in do_unpin_ids_submitted:
                do_unpin_ids.append(entry)


        for entry_id in do_pin_ids:
            shelf_details = shelf_util.edit_shelf_details_pin_by_id(int(entry_id))
            cache_update.shelf_attributes_updated(shelf_details)


        for entry_id in do_unpin_ids:
            shelf_details = shelf_util.edit_shelf_details_unpin_by_id(int(entry_id))
            cache_update.shelf_attributes_updated(shelf_details)

        return



    def process_form_shelf_edit(self, original_shelf_details, post_inputs, post_errors):
        
        redirect_url = None
        user_username = original_shelf_details.creator_username
        shelf_id = original_shelf_details.id

        shelf_name = string_func.removeEndSpace(request.form.get('shelf_name'))
            
        if 'pin_to_sidebar' in request.form:
            post_inputs['attributes']['pin'] = '1'
        else:
            post_inputs['attributes']['pin'] = '0'

        visibility = request.form.get('visibility')

        if visibility.lower() == 'private':
            post_inputs['attributes']['visibility'] = 'private'
        else:
            post_inputs['attributes']['visibility'] = 'public'

        if shelf_name != original_shelf_details.name:
            new_shelf_name = shelf_name
        else:
            new_shelf_name = None

        if post_inputs['attributes']['pin'] != original_shelf_details.attributes['pin']:
            new_pin_status = post_inputs['attributes']['pin']
        else:
            new_pin_status = None

        if post_inputs['attributes']['visibility'] != original_shelf_details.attributes['visibility']:
            new_visibility = post_inputs['attributes']['visibility']
        else:
            new_visibility = None

        if not new_shelf_name and not new_pin_status and not new_visibility:
            flash(u'No changes were detected.', u'notice')
        else:

            if new_shelf_name:
                post_inputs['shelf_name'] = new_shelf_name


                # validate form

                # do inputs fall within length range?
                if new_shelf_name and len(new_shelf_name) > constraints.SHELF_NAME_MAX_CHARS:
                    post_errors['shelf_name'] = u'That name is too long. Max: %s characters.' % constraints.SHELF_NAME_MAX_CHARS


                # if a new shelf name was provided, slugify it and confirm it does not contain all removed chars
                if new_shelf_name:
                    base_slug = slugify(new_shelf_name)

                    if base_slug == None:
                        post_errors['shelf_name'] = messages.SHELF_INVALID_SLUG



                # if no errors, generate shelf name slug
                if not post_errors.get('shelf_name'):

                    base_slug = slugify(new_shelf_name)

                    actual_slug = base_slug

                    duplicate_slug_count = shelf_util.count_shelves_matching_username_and_slug(user_username, base_slug)

                    if duplicate_slug_count > 1:
                        actual_slug = base_slug + '_%s' % duplicate_slug_count
                        new_shelf_name = new_shelf_name + ' (%s)' % duplicate_slug_count

                    # if everything passes, apply the updates

                    updated_shelf_details = rename_op.modify_shelf_name(original_shelf_details,
                                                                         new_shelf_name,
                                                                         base_slug,
                                                                         actual_slug)


                    if not updated_shelf_details:
                        flash(u'Something went wrong while renaming your shelf.', u'error')
                    else:
                        redirect_url = url_for('shelfBP.listing',
                                    user_username=user_username)

            if new_pin_status or new_visibility:

                shelf_details = shelf_util.edit_shelf_details_pin_and_visibility(int(shelf_id), new_pin_status, new_visibility)
                cache_update.shelf_attributes_updated(shelf_details)

                redirect_url = url_for('shelfBP.listing',
                                    user_username=user_username)

            flash(u'Changes saved.', u'success')

        return redirect_url, post_inputs, post_errors


    def process_form_shelf_rename(self, original_shelf_details, post_inputs, post_errors):
        shelf_name = string_func.removeEndSpace(request.form.get('shelf_name'))


        if shelf_name != original_shelf_details.name:
            new_shelf_name = shelf_name
        else:
            new_shelf_name = None


        if not new_shelf_name:
            flash(u'A new shelf name was not provided.', u'notice')
        else:
            post_inputs['shelf_name'] = new_shelf_name


            # validate form

            # do inputs fall within length range?
            if new_shelf_name and len(new_shelf_name) > constraints.SHELF_NAME_MAX_CHARS:
                post_errors['shelf_name'] = u'That name is too long. Max: %s characters.' % constraints.SHELF_NAME_MAX_CHARS


            # if a new shelf name was provided, slugify it and confirm it does not contain all removed chars
            if new_shelf_name:
                base_slug = slugify(new_shelf_name)

                if base_slug == None:
                    post_errors['shelf_name'] = messages.SHELF_INVALID_SLUG



            # if no errors, generate shelf name slug
            if not post_errors.get('shelf_name'):

                base_slug = slugify(new_shelf_name)

                actual_slug = base_slug

                duplicate_slug_count = shelf_util.count_shelves_matching_username_and_slug(user_username, base_slug)

                if duplicate_slug_count > 1:
                    actual_slug = base_slug + '-%s' % duplicate_slug_count
                    shelf_name = shelf_name + '-%s' % duplicate_slug_count

                # if everything passes, apply the updates

                updated_shelf_details = rename_op.modify_shelf_name(original_shelf_details,
                                                                     new_shelf_name,
                                                                     base_slug,
                                                                     actual_slug)


                if not updated_shelf_details:
                    flash(u'Something went wrong while renaming your shelf.', u'error')
                else:
                    redirect_url =  url_for('shelfBP.details',
                                             user_username=session['user_username'],
                                             shelf_slug=updated_shelf_details.actual_slug)



class BookForms(object):

    def __init__(self):
        pass


    def process_form_new_book(self):

        # if spam field wasn't empty, redirect to wherever
        # if request.form.get('email') != None:
        #     flash(u'You have been flagged as a spam bot.', u'notice')
        #     return redirect(url_for('frontpageBP.index'))



        # set empty dictionaries in case needed
        post_inputs={}
        post_errors={}


        # set empty success_url to return if not triggered

        success_url = None

        # get user_username from session
        user_username = session['user_username']


        # clean form fields and assign to variables
        book_name = string_func.removeEndSpace(request.form.get('book_name'))
        shelf = request.form.get('shelf')
        new_shelf_name = request.form.get('new_shelf_name')


        # populate dictionaries with form fields or errors if required
        if book_name:
            post_inputs['book_name'] = book_name
        else:
            post_errors['book_name'] = u'You must provide a book name.'

        post_inputs['shelf'] = shelf

        if new_shelf_name:
            post_inputs['new_shelf_name'] = new_shelf_name

        if shelf == 'new_shelf' and not new_shelf_name:
            post_errors['new_shelf_name'] = u'This options requires a new shelf name.'


        # if a book name was provided, slugify it and confirm it does not contain all removed chars
        if book_name:
            base_slug = slugify(book_name)

            if base_slug == None:
                post_errors['book_name'] = messages.BOOK_INVALID_SLUG



        ## validate the form submissions meets standard criteria


        # do inputs fall within character length range?
        if book_name and len(book_name) > constraints.BOOK_NAME_MAX_CHARS:
            post_errors['book_name'] = u'That name is too long. Max: %s characters.' % constraints.BOOK_NAME_MAX_CHARS

        # if no errors, and a new shelf is request, attempt to create it now
        if shelf == 'new_shelf' and not post_errors.get('new_shelf_name'):
            shelf_post_inputs, shelf_post_errors, shelf_success_url, new_shelf_details = shelf_forms.process_form_shelf_new(new_shelf_name)
        else:
            shelf_post_inputs, shelf_post_errors, shelf_success_url, new_shelf_details = None, None, None, None

        # if shelf creation succeeded, assign it to the post_inputs in case book creation fails
        if new_shelf_details:
            post_inputs['shelf'] = new_shelf_details.actual_slug
            post_inputs.pop('new_shelf_name', None)
            shelf = new_shelf_details.actual_slug
        # if shelf creation failed, assign it's error to the book errors
        elif shelf_post_errors:
            post_errors['new_shelf_name'] = shelf_post_errors.get('shelf_name')

        # if no errors, generate book name slug
        if not post_errors.get('book_name') and not post_errors.get('new_shelf_name'):

            base_slug = slugify(book_name)

            actual_slug = base_slug

            duplicate_slug_count = book_util.count_books_matching_username_shelf_and_slug(user_username, shelf, base_slug)

            if duplicate_slug_count > 1:
                actual_slug = base_slug + '-%s' % duplicate_slug_count
                book_name = book_name + '-%s' % duplicate_slug_count

            # verify slug name does not already exist. insert error if so

            # if everything passes, insert new book in BookDB

            new_book_details = book_func.insert_new_book_into_db(
                                                            book_name,
                                                            shelf,
                                                            base_slug,
                                                            actual_slug
                                                           )

            if not new_book_details:
                flash(u'Something went wrong while creating your book', u'error')
            else:
                # if successfull, will redirect to book details page
                success_url = url_for('bookBP.details',
                                       user_username=session['user_username'],
                                       shelf_slug=new_book_details.shelf_slug,
                                       book_slug=new_book_details.actual_slug)


        return post_inputs, post_errors, success_url


    def process_form_change_book_shelf(self, original_book_details):

        # set empty dictionaries in case needed
        post_inputs={}
        post_errors={}


        # set empty success_url to return if not triggered|
        success_url = None


        # get user_username from session
        user_username = session['user_username']


        # clean form fields and assign to variables
        shelf = request.form.get('shelf')
        new_shelf_name = request.form.get('new_shelf_name')


        # populate dictionaries with form fields or errors
        post_inputs['shelf'] = shelf

        if new_shelf_name:
            post_inputs['new_shelf_name'] = new_shelf_name

        if shelf == 'new_shelf' and not new_shelf_name:
            post_errors['new_shelf_name'] = u'This options requires a new shelf name.'

        if new_shelf_name and shelf != 'new_shelf':
            post_errors['new_shelf_name'] = u'You entered a new name but did not select to use it.'


        # # form validation

        # is 'new' shelf same as the current shelf?
        if shelf == original_book_details.shelf_slug:
            post_errors['shelf'] = messages.SHELF_NOT_NEW
            flash(messages.SHELF_NOT_NEW, u'notice')


        # if a new shelf name was provided, slugify it and confirm it does not contain all removed chars
        if new_shelf_name:
            base_slug = slugify(new_shelf_name)
            actual_slug = base_slug

            if base_slug == None:
                post_errors['new_shelf_name'] = messages.SHELF_INVALID_SLUG

            duplicate_slug_count = shelf_util.count_shelves_matching_username_and_slug(user_username, base_slug)

            if duplicate_slug_count > 1:
                actual_slug = base_slug + '-%s' % duplicate_slug_count
                new_shelf_name = new_shelf_name + '-%s' % duplicate_slug_count


        # if 'new shelf' is selected and provided shelf name passes validation, create the shelf
        if shelf == 'new_shelf' and not post_errors.get('new_shelf_name'):
            new_shelf_details = shelf_util.db_insert_new_shelf(new_shelf_name, base_slug, actual_slug)
            cache_update.user_creates_new_shelf(new_shelf_details.creator_username, new_shelf_details)

            post_inputs['shelf'] = new_shelf_details.name
            shelf = new_shelf_details.actual_slug


        # if no errors, proceed with shelf change
        if len(post_errors) == 0:

            shelf_details = shelf_util.get_shelf_details_by_username_and_shelf_slug(user_username, shelf)

            # now modify the shelf of book and perform cache updates
            new_book_details = book_util.db_mod_book_shelf_change(original_book_details, shelf_details)
            cache_update.book_shelf_modified(original_book_details, new_book_details)

            # subtract 1 form the old shelf's book count and update caches


            # get list of book's jots, update each with new shelf, and perform cache updates
            jot_listing = jot_util.lookup_jot_listing_by_book_slug(original_book_details.creator_username,
                                                                   original_book_details.shelf_slug,
                                                                   original_book_details.actual_slug)
            for jot in jot_listing:
                new_jot_details = jot_util.db_modify_jots_shelf_slug_and_name(jot, shelf_details.actual_slug, shelf_details.name)
                jot_util.cache_update_jot_details(new_jot_details)
            cache_update.jots_shelf_updated(original_book_details, new_book_details)

            success_url = url_for('bookBP.details',
                                   user_username=new_book_details.creator_username,
                                   shelf_slug=new_book_details.shelf_slug,
                                   book_slug=new_book_details.actual_slug)


        return post_inputs, post_errors, success_url


    def process_book_listing_form_submission_to_pin_and_unpin(self):

        do_pin_ids = request.form.getlist("do_pin")
        do_unpin_ids = []

        do_unpin_ids_submitted = request.form.getlist("do_unpin")
        pind_list_ids = request.form.getlist("pind_list")

        for entry in pind_list_ids:
            if entry not in do_unpin_ids_submitted:
                do_unpin_ids.append(entry)

        for entry_id in do_pin_ids:
            book_details = book_util.edit_book_details_pin_by_id(int(entry_id))
            cache_update.book_attributes_updated(book_details)

        for entry_id in do_unpin_ids:
            book_details = book_util.edit_book_details_unpin_by_id(int(entry_id))
            cache_update.book_attributes_updated(book_details)

        return
        return


class JotForms(object):

    def __init__(self):
        pass


    def process_form_new_or_edit_jot(self, user_username, shelf_slug, book_slug, jot_details=None):

        # verify the POSTer is authorized to be here

        if session.get('user_username') != user_username:
            flash(u'You are not authorized to POST here.', u'notice')
            redirect_url = url_for('bookBP.details',
                                    user_username=user_username,
                                    shelf_slug=shelf_slug,
                                    book_slug=book_slug)
        else:

            # set form submissions as variables (stripped of spaces)

            message = string_func.removeEndSpace(request.form.get('message'))
            form_attachments = request.form.getlist('attachment')

            # strip attachment fields and add to list to be converted to dictionary (for HSTORE)

            attachments = []

            for e in form_attachments:
                stripped_e = string_func.removeEndSpace(e)
                if stripped_e != None:
                    attachments.append(stripped_e)

            # if there is no message and no attachments, do not proceed

            if message == None and len(attachments) == 0:
                flash(u'Jot and attachment cannot be empty.', u'notice')
                redirect_url = url_for('bookBP.details',
                                        user_username=user_username,
                                        shelf_slug=shelf_slug,
                                        book_slug=book_slug)

            else:

                # if jot_details where provided, this is an edit of an existing jot

                if jot_details:

                    # convert the attachments list to a dictionary (for HSTORE)

                    attachments_dict = jot_util.create_jot_attachments_dict_from_list(attachments)

                    # determine if changes where made to the attachment fields. this is to prevent
                    # submissions from hitting the database if not changes where made to the jot

                    attachment_dict_change_count = jot_util.compare_jot_attachments_dict_and_return_change_count(jot_details.attachments, attachments_dict)

                    # if the message or attachments didn't change, do not proceed

                    if jot_details.message == message and attachment_dict_change_count == 0:
                        flash(u'You did not change the message or attachments.', u'notice')
                        redirect_url = None
                        return

                    # if the message or attachments changed, save the old details (temporarly) and
                    # modify fields to new values
                    else:
                        original_jot_details = jot_details
                        new_jot_details = jot_details
                        new_jot_details.message = message
                        new_jot_details.attachments = attachments_dict

                        # modify the database with new jot record and update effected caches

                        jot_save_status = jot_func.edit_jot_with_cache_updates(original_jot_details, new_jot_details)

                        # if successfully modified jot, flash and return
                        if jot_save_status:
                            flash(u'Jot update saved.', u'success')
                            redirect_url = url_for('jotBP.edit',
                                                    user_username=user_username,
                                                    shelf_slug=shelf_slug,
                                                    book_slug=book_slug,
                                                    jot_id=jot_details.id)
                            
                        # otherwise, something went wrong while creating/editing
                        else:
                            flash(u'Your jot could not be saved at this time.', u'error')
                            redirect_url = None


                # if no jot_details where provided, this is a new jot
                else:

                    # insert the new record into database
                    jot_save_status = jot_func.insert_new_jot_into_db(shelf_slug,
                                                                  book_slug,
                                                                  message,
                                                                  attachments)

                    # if successfully created, flash and return
                    if jot_save_status:
                        flash(u'Jot saved successfully.', u'success')
                        redirect_url = url_for('bookBP.details',
                                                user_username=user_username,
                                                shelf_slug=shelf_slug,
                                                book_slug=book_slug)
                        
                    # otherwise, something went wrong while creating/editing
                    else:
                        flash(u'Your jot could not be saved at this time.', u'error')
                        redirect_url = None

        return redirect_url


