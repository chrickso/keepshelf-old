# # -*- coding: utf-8 -*-

from flask import url_for

from ksapp.mods.shelf.utils import ShelfUtils
from ksapp.mods.book.utils import BookUtils
from ksapp.mods.jot.utils import JotUtils

from ksapp.etc.cache.keys import CacheKeys

from ksapp.extensions import redis

cachekey = CacheKeys()

shelf_util = ShelfUtils()
book_util = BookUtils()
jot_util = JotUtils()



class ActivityUpdates(object):

    def __init__(self):
        pass
    

    def user_creates_a_shelf(self, shelf_details):

        # get the next available activity id
        activity_id = self.get_next_activity_id()

        # create a redis hash of the activity details
        self.create_redis_hash_of_new_shelf_activity(activity_id, shelf_details)

        # add the activity_id to the global activity list
        self.insert_activity_id_into_global_activity_stream(activity_id)

        return


    def user_creates_a_book(self, book_details):

        # get the next available activity id
        activity_id = self.get_next_activity_id()

        # create a redis hash of the activity details
        self.create_redis_hash_of_new_book_activity(activity_id, book_details)

        # add the activity_id to the global activity list
        self.insert_activity_id_into_global_activity_stream(activity_id)

        return


    def user_creates_a_jot(self, jot_details):

        # get the next available activity id
        activity_id = self.get_next_activity_id()

        # create a redis hash of the activity details
        self.create_redis_hash_of_new_jot_activity(activity_id, jot_details)

        # add the activity_id to the global activity list
        self.insert_activity_id_into_global_activity_stream(activity_id)

        return


    def get_next_activity_id(self):
        next_id = redis.incr(cachekey.GLOBAL_ACTIVITY_STREAM_NEXT_ID)
        return next_id


    def insert_activity_id_into_global_activity_stream(self, activity_id):
        redis.lpush(cachekey.GLOBAL_ACTIVITY_STREAM_LIST_OF_ACTIVITY_IDS, activity_id)
        return True


    def create_redis_hash_of_new_shelf_activity(self, activity_id, shelf_details):
        activity_key = (cachekey.ACTIVITY_DETAILS % activity_id).encode('utf-8')
        activity_time = shelf_details.time_created
        
        username = shelf_details.creator_username
        shelf_slug = shelf_details.actual_slug
        shelf_name = shelf_details.name

        activity_message = u'<a href="%(username_profile_url)s">%(username)s</a> created shelf <a href="%(shelf_url)s">%(shelf_name)s</a>.' % {
                        'username_profile_url': u'#',
                        'username': username,
                        'shelf_url': url_for('shelfBP.details', user_username=username, shelf_slug=shelf_slug),
                        'shelf_name': shelf_name,
                        }


        redis.hmset(activity_key, {'message': activity_message, 'time': activity_time})

        return



    def create_redis_hash_of_new_book_activity(self, activity_id, book_details):
        activity_key = (cachekey.ACTIVITY_DETAILS % activity_id).encode('utf-8')
        activity_time = book_details.time_created
        
        username = book_details.creator_username
        book_name = book_details.name
        book_slug = book_details.actual_slug
        shelf_slug = book_details.shelf_slug
        shelf_name = book_details.shelf_name
        
        activity_message = u'<a href="%(username_profile_url)s">%(username)s</a> created book <a href="%(book_url)s">%(book_name)s</a> | <a href="%(shelf_url)s">%(shelf_name)s</a>.' % {
                        'username_profile_url': u'#',
                        'username': username,
                        'book_url': url_for('bookBP.details', user_username=username, shelf_slug=shelf_slug, book_slug=book_slug),
                        'book_name': book_name,
                        'shelf_url': url_for('shelfBP.details', user_username=username, shelf_slug=shelf_slug),
                        'shelf_name': shelf_name,
                        }


        redis.hmset(activity_key, {'message': activity_message, 'time': activity_time})

        return


    def create_redis_hash_of_new_jot_activity(self, activity_id, jot_details):
        activity_key = (cachekey.ACTIVITY_DETAILS % activity_id).encode('utf-8')
        activity_time = jot_details.time_created
        
        username = jot_details.creator_username
        jot_id = jot_details.id
        book_name = jot_details.book_name
        book_slug = jot_details.book_slug
        shelf_slug = jot_details.shelf_slug
        shelf_name = jot_details.shelf_name
        
        activity_message = u'<a href="%(username_profile_url)s">%(username)s</a> <a href="%(jot_url)s">jotted</a> in book <a href="%(book_url)s">%(book_name)s</a> | <a href="%(shelf_url)s">%(shelf_name)s</a>.' % {
                        'username_profile_url': u'#',
                        'username': username,
                        'jot_url': url_for('jotBP.details', user_username=username, shelf_slug=shelf_slug, book_slug=book_slug, jot_id=jot_id),
                        'book_url': url_for('bookBP.details', user_username=username, shelf_slug=shelf_slug, book_slug=book_slug),
                        'book_name': book_name,
                        'shelf_url': url_for('shelfBP.details', user_username=username, shelf_slug=shelf_slug),
                        'shelf_name': shelf_name,
                        }


        redis.hmset(activity_key, {'message': activity_message, 'time': activity_time})

        return


    
