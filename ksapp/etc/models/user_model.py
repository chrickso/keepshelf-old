# -*- coding: utf-8 -*-

from werkzeug import cached_property

from ksapp.extensions import db, bcrypt
from ksapp.etc.utils.time import TimeUtils
from ksapp.etc.constraints import Constraints

from sqlalchemy.dialects.postgresql import HSTORE
from sqlalchemy.ext.mutable import MutableDict


time_utils = TimeUtils()
constraints = Constraints()

VARCHAR_LEN_128 = 128


class UserDB(db.Model):

	__tablename__ = 'users'

	id = db.Column(db.Integer, primary_key=True)

	username = db.Column(db.String(constraints.USERS_USERNAME_MAX_CHARS), nullable=False, unique=True, index=True)
	_password = db.Column('password', db.String(VARCHAR_LEN_128), nullable=False)

	creator_ip = db.Column(db.String(VARCHAR_LEN_128), nullable=True)
	created_time = db.Column(db.DateTime, default=time_utils.get_current_time)

	email_contact = db.Column(db.String(constraints.USERS_MAX_EMAIL_CHARS), nullable=True)

	contact = db.Column(MutableDict.as_mutable(HSTORE), nullable=False, default={}) # twitter: username, faceshelf: username

	settings = db.Column(MutableDict.as_mutable(HSTORE), nullable=False, default={})



	def _get_password(self):
		return self._password

	def _set_password(self, password):
		self._password = bcrypt.generate_password_hash(password)

	# Hide password encryption by exposing password field only.
	password = db.synonym('_password',
						  descriptor=property(_get_password,
											  _set_password))

	def check_password(self, password):
		if self.password is None:
			return False
		return bcrypt.check_password_hash(self.password, password)


	@classmethod
	def authenticate(cls, username, password):
		user = cls.query.filter(db.func.lower(UserDB.username)==db.func.lower(username)).first()

		if user:
			authenticated = user.check_password(password)
		else:
			authenticated = False

		return user, authenticated


	@classmethod
	def username_taken(cls, username):
		u = cls.query.filter(db.func.lower(UserDB.username)==db.func.lower(username)).first()

		if u:
			return True
		else:
			return False


		

	# @classmethod
	# def search(cls, keywords):
	#     criteria = []
	#     for keyword in keywords.split():
	#         keyword = '%' + keyword + '%'
	#         criteria.append(db.or_(
	#             UserDB.name.ilike(keyword),
	#             UserDB.email.ilike(keyword),
	#         ))
	#     q = reduce(db.and_, criteria)
	#     return cls.query.filter(q)
