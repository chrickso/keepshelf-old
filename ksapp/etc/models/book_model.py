# -*- coding: utf-8 -*-

from ksapp.extensions import db
# from ksapp.mods.etc.utils.db import DenormalizedText, VARCHAR_LEN_128
from ksapp.etc.utils.time import TimeUtils
from ksapp.etc.constraints import Constraints

from sqlalchemy.dialects.postgresql import HSTORE
from sqlalchemy.ext.mutable import MutableDict


time_utils = TimeUtils()
constraints = Constraints()

VARCHAR_LEN_128 = 128


class BookDB(db.Model):

    __tablename__ = 'books'    

    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(constraints.SHELF_NAME_MAX_CHARS), nullable=False)

    creator_id = db.Column(db.Integer, nullable=False)
    creator_username = db.Column(db.String(constraints.USERS_USERNAME_MAX_CHARS), nullable=False, index=True)

    time_created = db.Column(db.DateTime, default=time_utils.get_current_time)
    last_modified = db.Column(db.DateTime, default=time_utils.get_current_time)

    base_slug = db.Column(db.String(constraints.SLUGS_MAX_SLUG_CHARS), nullable=False)
    actual_slug = db.Column(db.String(constraints.SLUGS_MAX_SLUG_CHARS), nullable=False, index=True)

    shelf_name = db.Column(db.String(constraints.SHELF_NAME_MAX_CHARS), nullable=False)
    shelf_slug = db.Column(db.String(constraints.SLUGS_MAX_SLUG_CHARS), nullable=False, index=True)
    shelf_attributes = db.Column(MutableDict.as_mutable(HSTORE), nullable=False, default={ 'share_level' : 'public', 'disabled' : '0'})

    users_who_starred = db.Column(MutableDict.as_mutable(HSTORE), nullable=False, default={})
    users_who_rebooked = db.Column(MutableDict.as_mutable(HSTORE), nullable=False, default={})

    attributes = db.Column(MutableDict.as_mutable(HSTORE), nullable=False, default={ 'pin' : '0', 'private' : '0', 'share_level' : '0', 'disabled' : '0'}, index=True) # pin'd, private, share_level, disable
    stats = db.Column(MutableDict.as_mutable(HSTORE), nullable=False, default={ 'jot_count' : '0' }) # jot_count, likes, copies, etc



    def __init__(self,
                 name=None,
                 creator_id=None,
                 creator_username=None,
                 last_modified=None,
                 base_slug=None,
                 actual_slug=None,
                 shelf_name=None,
                 shelf_slug=None,
                 shelf_attributes=None,
                 ):
            self.name = name
            self.creator_id = creator_id
            self.creator_username = creator_username
            self.last_modified = last_modified
            self.base_slug = base_slug
            self.actual_slug = actual_slug
            self.shelf_name = shelf_name
            self.shelf_slug = shelf_slug
            self.shelf_attributes = shelf_attributes

            