# -*- coding: utf-8 -*-

from ksapp.extensions import db
# from ksapp.mods.etc.utils.db import DenormalizedText, VARCHAR_LEN_128
from ksapp.etc.utils.time import TimeUtils
from ksapp.etc.constraints import Constraints

from sqlalchemy.dialects.postgresql import HSTORE
from sqlalchemy.ext.mutable import MutableDict


time_utils = TimeUtils()
constraints = Constraints()

VARCHAR_LEN_128 = 128


class ShelfDB(db.Model):

    __tablename__ = 'shelves'    

    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(constraints.SHELF_NAME_MAX_CHARS), nullable=False)

    creator_id = db.Column(db.Integer, nullable=False)
    creator_username = db.Column(db.String(constraints.USERS_USERNAME_MAX_CHARS), nullable=False, index=True)

    time_created = db.Column(db.DateTime, default=time_utils.get_current_time)
    last_modified = db.Column(db.DateTime, default=time_utils.get_current_time)

    base_slug = db.Column(db.String(constraints.SLUGS_MAX_SLUG_CHARS), nullable=False)
    actual_slug = db.Column(db.String(constraints.SLUGS_MAX_SLUG_CHARS), nullable=False, index=True)

    users_watching = db.Column(MutableDict.as_mutable(HSTORE), nullable=False, default={})

    attributes = db.Column(MutableDict.as_mutable(HSTORE), nullable=False, default={ 'pin' : '0', 'visibility' : 'public', 'disabled' : '0'}, index=True) # pin'd, private, share_level, disable
    stats = db.Column(MutableDict.as_mutable(HSTORE), nullable=False, default={ 'book_count' : '0' }) # jot_count, likes, copies, etc





    def __init__(self,
                 name=None,
                 creator_id=None,
                 creator_username=None,
                 base_slug=None,
                 actual_slug=None,
                 attributes=None):
            self.name = name
            self.creator_id = creator_id
            self.creator_username = creator_username
            self.base_slug = base_slug
            self.actual_slug = actual_slug
            self.attributes = attributes

            