# -*- coding: utf-8 -*-

from ksapp.extensions import db
# from ksapp.mods.etc.utils.db import DenormalizedText, VARCHAR_LEN_128
from ksapp.etc.utils.time import TimeUtils
from ksapp.etc.constraints import Constraints

from sqlalchemy.dialects.postgresql import HSTORE
from sqlalchemy.ext.mutable import MutableDict


time_utils = TimeUtils()
constraints = Constraints()

VARCHAR_LEN_128 = 128


class JotDB(db.Model):

    __tablename__ = 'jots'    

    id = db.Column(db.Integer, primary_key=True)

    message = db.Column(db.Text, nullable=True)

    creator_id = db.Column(db.Integer, nullable=False)
    creator_username = db.Column(db.String(constraints.USERS_USERNAME_MAX_CHARS), nullable=False, index=True)

    time_created = db.Column(db.DateTime, default=time_utils.get_current_time)
    last_modified = db.Column(db.DateTime, default=time_utils.get_current_time)

    shelf_name = db.Column(db.String(constraints.SHELF_NAME_MAX_CHARS), nullable=False)
    shelf_slug = db.Column(db.String(constraints.SLUGS_MAX_SLUG_CHARS), nullable=False, index=True)
    shelf_attributes = db.Column(MutableDict.as_mutable(HSTORE), nullable=False, default={ 'share_level' : 'public', 'disabled' : '0'})

    book_name = db.Column(db.String(constraints.SHELF_NAME_MAX_CHARS), nullable=False)
    book_slug = db.Column(db.String(constraints.SLUGS_MAX_SLUG_CHARS), nullable=False, index=True)
    book_attributes = db.Column(MutableDict.as_mutable(HSTORE), nullable=False, default={ 'share_level' : 'public', 'disabled' : '0'})

    attachments = db.Column(MutableDict.as_mutable(HSTORE), nullable=False, default={}, index=True)

    attributes = db.Column(MutableDict.as_mutable(HSTORE), nullable=False, default={}) # striked

    users_who_rejotted = db.Column(MutableDict.as_mutable(HSTORE), nullable=False, default={})
    users_who_starred = db.Column(MutableDict.as_mutable(HSTORE), nullable=False, default={})

    reply_count = db.Column(db.Integer, nullable=False, default=0)

    share_level = db.Column(db.Integer, nullable=False, default=1, index=True) # 1 = Public, 0 = Private

    active = db.Column(db.Integer, nullable=False, default=1, index=True) # 0 = No, 1 = Yes


    def __init__(self,
                 message=None,
                 creator_id=None,
                 creator_username=None,
                 last_modified=None,
                 shelf_name=None,
                 shelf_slug=None,
                 shelf_attributes=None,
                 book_name=None,
                 book_slug=None,
                 book_attributes=None,
                 attachments=None,
                 share_level=None):
            self.message = message
            self.creator_id = creator_id
            self.creator_username = creator_username
            self.last_modified = last_modified
            self.shelf_name = shelf_name
            self.shelf_slug = shelf_slug
            self.shelf_attributes = shelf_attributes
            self.book_name = book_name
            self.book_slug = book_slug
            self.book_attributes = book_attributes
            self.attachments = attachments
            self.share_level = share_level



            