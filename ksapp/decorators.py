# -*- coding: utf-8 -*-

from functools import wraps, update_wrapper

# from flask import (g, url_for, flash, redirect, Markup, request,
# 					session, make_response, request, current_app)


from flask import session, current_app, render_template

from ksapp.mods.user.functions import UserFunctions

user_func = UserFunctions()

# app = current_app

# def include_sidebar_data(func):
#     @wraps(func)
#     @app.context_processor
#     def sidebar_data_to_jinja(*args, **kwargs):
#         sidebar_shelf_listing, sidebar_book_listing = user_func.get_sidebar_listings()
#         return sidebar_shelf_listing, sidebar_page_listing

# def include_sidebar_data(fn):
#     template_name = fn()
#     sidebar_shelf_listing, sidebar_book_listing = user_func.get_sidebar_listings()
#     def wrapped():
#         return render_template(template_name,
#                                sidebar_shelf_listing=sidebar_shelf_listing,
#                                sidebar_book_listing=sidebar_page_listing)
#     return wrapped


def render_sidebar_template(tmpl_name, **kwargs):
    sidebar_shelf_listing, sidebar_book_listing = user_func.get_sidebar_listings()
    return render_template(tmpl_name,
                           sidebar_shelf_listing=sidebar_shelf_listing,
                           sidebar_book_listing=sidebar_page_listing,
                           **kwargs)



# def keep_login_url(func):
#     """
#     Adds attribute g.keep_login_url in order to pass the current
#     login URL to login/signup links.
#     """
#     @wraps(func)
#     def wrapper(*args, **kwargs):
#         g.keep_login_url = True
#         return func(*args, **kwargs)
#     return wrapper


# def login_required(f):
#     """
#     Return to user login page with flash message if 'user_username' not in session.
#     """
#     @wraps(f)
#     def decorated_function(*args, **kwargs):
#         if not session.get('user_username'):
#             next = request.url
#             flash(u'You must be logged in to access that page.', u'error')
#             return redirect(url_for('userBP.login', next=next))
#         return f(*args, **kwargs)
#     return decorated_function





# @crossdomain decorator
# Decorator for the HTTP Access Control
# http://flask.pocoo.org/snippets/56/
# this is needed because logging in via notification popup accesses
# a different domain (userBP.domain.com/login)

# def crossdomain(origin=None, methods=None, headers=None,
#                 max_age=21600, attach_to_all=True,
#                 automatic_options=True):
#     if methods is not None:
#         methods = ', '.join(sorted(x.upper() for x in methods))
#     if headers is not None and not isinstance(headers, basestring):
#         headers = ', '.join(x.upper() for x in headers)
#     if not isinstance(origin, basestring):
#         origin = ', '.join(origin)
#     if isinstance(max_age, timedelta):
#         max_age = max_age.total_seconds()

#     def get_methods():
#         if methods is not None:
#             return methods

#         options_resp = current_app.make_default_options_response()
#         return options_resp.headers['allow']

#     def decorator(f):
#         def wrapped_function(*args, **kwargs):
#             if automatic_options and request.method == 'OPTIONS':
#                 resp = current_app.make_default_options_response()
#             else:
#                 resp = make_response(f(*args, **kwargs))
#             if not attach_to_all and request.method != 'OPTIONS':
#                 return resp

#             h = resp.headers

#             h['Access-Control-Allow-Origin'] = origin
#             h['Access-Control-Allow-Methods'] = get_methods()
#             h['Access-Control-Max-Age'] = str(max_age)
#             # h['Access-Control-Allow-Credentials'] = true
#             if headers is not None:
#                 h['Access-Control-Allow-Headers'] = headers
#             return resp

#         f.provide_automatic_options = False
#         return update_wrapper(wrapped_function, f)
#     return decorator