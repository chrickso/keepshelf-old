// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.


// get selected elements html
// http://stackoverflow.com/questions/2419749/get-selected-elements-outer-html?lq=1
    jQuery.fn.outerHTML = function(s) {
        return s
            ? this.before(s).remove()
            : jQuery("<p>").append(this.eq(0).clone()).html();
    };



// jPanelMenu
// http://jpanelmenu.com/

    // create instance and set options
    var jPM = $.jPanelMenu({
        menu: '#sidebar-wrap',
        trigger: '.sidebar-toggle',
        openPosition: '85%',
        duration: 350
    });

    // turn it Only
    jPM.on();