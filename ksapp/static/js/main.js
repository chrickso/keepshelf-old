// example / reference code:

    // e.preventDefault();
    // // if a link has class 'no-double-post', clicking it again should do nothing
    // if ( $(this).hasClass('no-double-post') ) {
    //     return false;
    // } else {
    //     $(this).addClass('no-double-post');
    // }
    // $(this).closest('form').submit();


// submit form when 'enter' key pressed in input box
// http://stackoverflow.com/questions/8981637/submit-form-with-enter-key
$(document).on('keypress', 'input', function(event) {
    if (event.which === 13) {
        event.preventDefault();
        $(this).closest('form').submit();
    }
});



// submit form if a text link with class of 'form-submit-button' is hit

$(document).on('click', 'a.form-submit-button', function(e) {
	e.preventDefault();
	$(this).closest('form').submit();
});





// book details - new jot form

// clicking the '+' attachment button will add another atachment field directly below
$(document).on('click', 'button.add-jot-attachment', function(e) {
	// console.log('working');

	var $attachment_field = $(e.target).closest('.row.jot-attachment-field');

	var a = $($attachment_field).outerHTML();
	var b = $($attachment_field).after(a);

	// also add the 'multi' class to the div.jot-attachment-wrap to help with the 'minus' button
	var $jot_attachment_wrap = $(e.target).closest('.jot-attachment-wrap');

	if ( !$jot_attachment_wrap.hasClass('multi') ) {
		$($jot_attachment_wrap).addClass('multi');
	}

});


// clicking the '-' attachment button will remove that field from the form, count the remaining
// fields, and remove the 'multi' class from 'jot-attachment-wrap' (to hide '-' buttons) if only 1

$(document).on('click', 'button.remove-jot-attachment', function(e) {

	var $attachment_field = $(e.target).closest('.row.jot-attachment-field');
	var $jot_attachment_wrap = $(e.target).closest('.jot-attachment-wrap');

	var a = $($attachment_field).remove();

	// if the number of jot-attachment-field's remaining == 1, remove the 'multi' class from
	// jot-attachment-wrap

	if ( $('.row.jot-attachment-field').length == 1 ) {
		$($jot_attachment_wrap).removeClass('multi');
	}

});