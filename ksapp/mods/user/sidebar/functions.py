# -*- coding: utf-8 -*-

from flask import session, current_app, render_template

from ksapp.mods.shelf.utils import ShelfUtils
from ksapp.mods.book.utils import BookUtils


shelf_util = ShelfUtils()
book_util = BookUtils()


def get_sidebar_listings():
        """Checks for a logged in user and returns listing of sidebar shelves/books if so."""
        if session.get('user_username'):
            username = session['user_username']
            sidebar_shelf_listing = shelf_util.get_shelf_listing_sidebar_by_username(username)
            sidebar_book_listing = book_util.get_book_listing_sidebar_by_username(username)
        else:
            sidebar_shelf_listing = None
            sidebar_book_listing = None

        return sidebar_shelf_listing, sidebar_book_listing




def render_sidebar_template(tmpl_name, **kwargs):
    sidebar_shelf_listing, sidebar_book_listing = get_sidebar_listings()
    return render_template(tmpl_name,
                           sidebar_shelf_listing=sidebar_shelf_listing,
                           sidebar_book_listing=sidebar_book_listing,
                           **kwargs)


