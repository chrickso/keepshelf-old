# -*- coding: utf-8 -*-

# from flask import session, flash, redirect, url_for

from ksapp.extensions import db
from ksapp.etc.models.user_model import UserDB


class UserFunctions(object):

    def __init__(self):
        pass


    def create_new_user(self, username=None, password=None, creator_ip=None, email=None):
            try:
                new_user = UserDB(
                                username = username,
                                password = password,
                                creator_ip = creator_ip,
                                email_contact = email
                              )
                db.session.add(new_user)

                # if hide_list, process

                db.session.commit()
                return True, new_user.id
            except:
                return False, None

