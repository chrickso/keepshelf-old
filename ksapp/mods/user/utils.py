# -*- coding: utf-8 -*-

from flask import session, flash, redirect, url_for, request

from pbapp.extensions import db
from pbapp.mods.user import UserDB

from pbapp.mods.etc.cache.setup import cache
from pbapp.mods.etc.cache.keys import CacheKeys


cachekey = CacheKeys()



class UserUtils(object):

    def __init__(self):
        pass


    def update_user_details_cache(self, user_username):
        k = (cachekey.QUERY_USER_DETAILS % user_username).encode('utf-8')
        v = UserDB.query.filter_by(username=user_username).first()
        cache.set(k, v)
        return True


    def get_user_details(self, user_username):
        k = (cachekey.QUERY_USER_DETAILS % user_username).encode('utf-8')
        v = cache.get(k)

        if v:
            if v == cachekey.NO_QUERY_RESULTS:
                return False
            else:
                return v
        else:
            v = UserDB.query.filter_by(username=user_username).first()
            if v:
                cache.set(k, v)
            else:
                cache.set(k, cachekey.NO_QUERY_RESULTS)
        return v



    def is_post_in_users_mute_list(self, user_username, post_id):
        """Returns True if post is muted, False if not."""
        user_details = self.get_user_details(user_username)
        if str(post_id) in user_details.muted_posts:
            return True
        else:
            return False


