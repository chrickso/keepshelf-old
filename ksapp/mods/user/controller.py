# -*- coding: utf-8 -*-

from flask import (Blueprint, request, render_template,
                   g, redirect, url_for, flash, session)

from ksapp.extensions import db
from ksapp.etc.models.user_model import UserDB

from ksapp.etc.utils.strings import StringFunctions
from ksapp.etc.utils.redirect import get_redirect_target, redirect_back

from ksapp.mods.user.functions import UserFunctions

from ksapp.mods.user.sidebar.functions import render_sidebar_template

from recaptcha.client import captcha


userBP = Blueprint('userBP', __name__)


string_func = StringFunctions()
user_func = UserFunctions()


@userBP.route('/u/<user_username>/')
def profile(user_username):
    next = url_for('userBP.profile', user_username=user_username)
    up_level_url = url_for('frontpageBP.index')

    if not session.get('user_username'):
        flash(u'You must be logged in for that.', u'notice')
        return redirect(url_for('userBP.login', next=next))

    return render_sidebar_template('mods/user/profile.html',
                            next=next,
                            up_level_url=up_level_url,
                            user_username=user_username
                            )


@userBP.route('/u/<user_username>/settings/')
def settings(user_username):
    next = url_for('userBP.profile', user_username=user_username)
    up_level_url = url_for('userBP.profile', user_username=user_username)

    if not session.get('user_username'):
        flash(u'You are not logged in as that user.', u'notice')
        return redirect(url_for('userBP.login', next=next))

    return render_sidebar_template('mods/user/settings.html',
                            next=next,
                            up_level_url=up_level_url,
                            user_username=user_username
                            )



@userBP.route('/login/', methods=['GET', 'POST'])
def login(signup_inputs={}, signup_errors={}):
    page_title = 'User Login'

    next = request.form.get('next')

    if not next:
        next = get_redirect_target()
    
    if not next or next == url_for('userBP.login', _external=True) \
                or next == url_for('userBP.logout', _external=True):
        next = url_for('frontpageBP.index')

    login_inputs = {}
    login_errors = {}

    # first, verify the user_username is not set. if it is, render user profile page.
    if session.get('user_username'):
        return redirect(request.values.get('next', url_for('userBP.profile', username=session['user_username'])))

    elif request.method == 'POST':

        username = string_func.no_whitespace(request.form.get('username')).lower()
        password = request.form.get('password')

        # may need more form validation before hitting the database

        if username and password:
            user, authenticated = UserDB.authenticate(username, password)

            # verify u/p
            if user and authenticated:
                session['user_id'] = user.id
                session['user_username'] = user.username
                session['default_share_level'] = user.settings['default_share_level']
                flash(u'You are now logged in as <strong>%s</strong>.' % user.username, u'success')
                return redirect_back('frontpageBP.index')

            # if verify u/p fails
            else:
                flash(u'That username/password is not in our system.', u'error')
        else:
            flash(u'Username and password were not provided.', u'notice')
            if username and not password:
                login_inputs['username'] = username
                login_errors['password'] = u'You did not provide a password.'

    return render_sidebar_template('mods/frontpage/index.html', page_title=page_title, next=next,
                            login_inputs=login_inputs, login_errors=login_errors,
                            signup_inputs=signup_inputs, signup_errors=signup_errors)



@userBP.route('/logout', methods=['POST'])
def logout():
    next = get_redirect_target()
    if session.get('user_username'):
        session.pop('user_username', None)
        if session.get('user_id'):
            session.pop('user_id', None)
        flash(u'You have logged out.', u'success')
    else:
        flash(u'You were not logged in, so you cannot log out.', u'Notice')
    return redirect_back('frontpageBP.index')



@userBP.route('/user/new/', methods=['GET', 'POST'])
def signup(login_inputs={}, login_errors={}):
    page_title = 'User'

    next = request.form.get('next')

    if not next:
        next = get_redirect_target()
    
    if not next or next == url_for('userBP.signup', _external=True) \
                or next == url_for('userBP.logout', _external=True):
        next = url_for('frontpageBP.index')


    signup_inputs={}
    signup_errors={}

    if session.get('user_username'):
        return redirect(request.values.get('next', url_for('userBP.profile', username=session['user_username'])))

    elif request.method == 'POST':

        username_input = string_func.no_whitespace(request.form.get('username'))
        if username_input:
            signup_inputs['username'] = username_input
            if len(username_input) < 6:
                signup_errors['username'] = u'Usernames must be at least 6 characters'
            elif len(username_input) > 15:
                signup_errors['username'] = u'Maximum username length is 15 characters.<br>We have trimmed it down for you.'
                signup_inputs['username'] = username_input[:15]
            else:
                if signup_inputs['username'] != string_func.sanitize_username(signup_inputs['username']):
                    signup_errors['username'] = u'Supported characters are: a-z, A-Z, 0-9, -, _, .'
        else:
            signup_errors['username'] = u'A username is required.'

        password_input = request.form.get('password')
        repeat_pass_input = request.form.get('repeat_pass')

        if not password_input:
            signup_errors['password'] = u'A password is required.'
        if password_input:
            if len(password_input) < 7:
                signup_errors['password'] = u'Passwords need to have at least 7 characters.'
            elif len(password_input) > 300:
                signup_errors['password'] = u'We do not accept passwords over 300 characters.'
            else:
                if password_input and not repeat_pass_input:
                    signup_errors['repeat_pass'] = u'You must repeat your password to ensure no mistypings.'
                if password_input and repeat_pass_input:
                    if password_input != repeat_pass_input:
                        signup_errors['password'] = u'Your password and repeat password did not match.'

        # verify captcha
        response = captcha.submit(
            request.form.get('recaptcha_challenge_field'),
            request.form.get('recaptcha_response_field'),
            '6LegndUSAAAAANhyV6sfczGnpInKKSG4cFo2BHKO', # private key
            request.remote_addr
        )

        if not response.is_valid:
            signup_errors['captcha'] = u'Your words did not match the shown image.'

        # if no errors up to this point, lookup the requested username to determine availability
        if not signup_errors:
            username_taken = UserDB.username_taken(username_input)
            if username_taken:
                signup_errors['username'] = u'This username is already taken.'

        # if everything checks out, create a new database entry
        if not signup_errors.get('username') and not signup_errors.get('password') \
           and not signup_errors.get('repeat_pass') and not signup_errors.get('tagline') \
           and not signup_errors.get('hide_list') and not signup_errors.get('captcha'):
            creator_ip = request.remote_addr
            created_new_user_status, new_user_id = user_func.create_new_user(username=username_input, password=password_input, creator_ip=creator_ip)

            if created_new_user_status == True:
                session['user_id'] = new_user_id
                session['user_username'] = username_input
                flash(u'New user created. You are now signed in as <strong>%s</strong>' % username_input, u'success')
                return redirect(url_for('frontpageBP.index'))
            else:
                flash(u'We had an issue creating this user. You did nothing wrong.', u'error')
        else:
            flash(u'New user was not created. See <span class="red">error(s)</span> below.', u'error')
            signup_errors['password'] = u'Sorry, you\'ll need to enter your password again'
            

    return render_sidebar_template('mods/user/signup.html', page_title=page_title, next=next,
                            login_inputs=login_inputs, login_errors=login_errors,
                            signup_inputs=signup_inputs, signup_errors=signup_errors)





# @userBP.route('/new', methods=['GET', 'POST'])
# def new_user():
#     form = SignupForm(next=request.args.get('next'))

#     if form.validate_on_submit():
#         user = UserDB()
#         form.populate_obj(user)

#         db.session.add(user)
#         db.session.commit()

#         # if login_user(user):
#         #     return redirect(form.next.data or url_for('userBP.profile_public'))
#         flash(_('New user created.'), 'error')

#     return render_template('mods/user/login/signup.html', form=form)

