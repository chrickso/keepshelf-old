# -*- coding: utf-8 -*-

from flask import flash, redirect, request, session, url_for

from ksapp.extensions import db
from ksapp.etc.models.book_model import BookDB

from ksapp.etc.cache.updates import CacheUpdates

from ksapp.mods.shelf.functions import ShelfFunctions

from ksapp.mods.shelf.utils import ShelfUtils
from ksapp.mods.book.utils import BookUtils
from ksapp.mods.jot.utils import JotUtils

from ksapp.etc.constraints import Constraints
from ksapp.etc.messages import Messages

from ksapp.etc.utils.strings import StringFunctions

from ksapp.etc.activity.updates import ActivityUpdates

from ksapp.etc.utils.slug import slugify

# # from pbapp.mods.etc.cache.setup import cache
from ksapp.etc.cache.keys import CacheKeys

# from pbapp.mods.etc.utils.post_utils import PostUtils

from ksapp.extensions import redis


cachekey = CacheKeys()
# post_util = PostUtils()

cache_update = CacheUpdates()
shelf_func = ShelfFunctions()
shelf_util = ShelfUtils()
book_util = BookUtils()
jot_util = JotUtils()
activity_update = ActivityUpdates()
string_func = StringFunctions()
constraints = Constraints()
messages = Messages()




class BookFunctions(object):

    def __init__(self):
        pass


    def insert_new_book_into_db(self,
                                book_name,
                                shelf_slug,
                                base_slug,
                                actual_slug,
                                user_id=None,
                                user_username=None):

        if user_id == None:
            user_id = session['user_id']

        if user_username == None:
            user_username == session['user_username']


        if session.get('default_share_level') == 'private':
            share_level = 0
        else:
            share_level = 1

        shelf_details = shelf_util.get_shelf_details_by_username_and_shelf_slug(user_username, shelf_slug)

        shelf_name = shelf_details.name

        new_book = BookDB(name=book_name,
                          creator_id=user_id,
                          creator_username=user_username,
                          shelf_name=shelf_name,
                          shelf_slug=shelf_slug,
                          base_slug=base_slug,
                          actual_slug=actual_slug)
        db.session.add(new_book)

        db.session.commit()

        # k = (cachekey.BOARD_LIST_QUERY %(place_url, type)).lower().encode('utf-8')
        # cache.delete(k)

        # k = (cachekey.BOARD_URL_EXISTS_CHECK % (place_url, board_url)).lower().encode('utf-8')
        # cache.set(k, True)

        # k = (cachekey.BOARD_NAME_EXISTS_CHECK % (place_url, string_func.sanitize_board_name(board_name))).lower().encode('utf-8')
        # cache.set(k, True)

        # # update the place_details cache because it has a new board_list entry
        # k = (cachekey.SINGLE_PLACE_QUERY % place_url).encode('utf-8')
        # cache.set(k, place_details)

        # #cache_update.new_board_cache_update(place_url, board_url, board_name)

        # perform cache updates for new book
        cache_update.user_creates_new_book(user_username, new_book)
        activity_update.user_creates_a_book(new_book)

        # add one to the shelf's book_count
        shelf_details = shelf_func.edit_shelf_details_add_1_to_book_count_and_update_caches(user_username, shelf_slug)

        return new_book


    def get_prev_and_next_book_in_shelf_slugs(self, username, shelf_slug, current_book_slug):
        key = (cachekey.LISTING_OF_SHELFS_BOOK_SLUGS_SORT_ALPHA % (username, shelf_slug)).encode('utf-8')

        # check if redis list exsists
        redis_list_exists_check = redis.exists(key)

        if redis_list_exists_check == 0:
            generate_sorted_set = book_util.update_prev_and_next_book_in_shelf_slugs_sorted_set(username, shelf_slug)

        # determine list size
        list_size = redis.zcard(key)

        # if only 1 book in shelf, set prev/next links to none. prev/next buttons will not be shown
        if list_size == 1:
            prev_book_in_shelf_slug = None
            next_book_in_shelf_slug = None
        else:
            # get the full list
            list_of_shelfs_book_slugs = redis.zrange(key, 0, -1)

            # determine current_book_slug's position in list
            current_book_slug = current_book_slug
            current_book_position = redis.zrank(key, current_book_slug)

            # set index values for next/prev
            prev_index = current_book_position - 1 # if 0 then -1 will pull the last entry

            # if current_book is last in the list
            if current_book_position == list_size - 1:
                # next link wraps to front
                next_index = 0
            else:
                next_index = current_book_position + 1

            # asign the links at specified index position to the returned vars
            prev_book_in_shelf_slug = (list_of_shelfs_book_slugs[prev_index]).decode('utf-8')
            next_book_in_shelf_slug = (list_of_shelfs_book_slugs[next_index]).decode('utf-8')

            # unicode(i, encoding='UTF-8')

        
        return prev_book_in_shelf_slug, next_book_in_shelf_slug


    def edit_book_details_add_1_to_jot_count_and_update_caches(self, username, shelf_slug, book_slug):
        # perform the database update
        updated_book_details = book_util.edit_book_details_add_1_to_jot_count(username, shelf_slug, book_slug)

        # perform the cache update
        cache_update.book_stat_updated(updated_book_details)

        return updated_book_details


    def edit_book_details_subtract_1_from_book_count_and_update_caches(self, username, shelf_slug, book_slug):
        # perform the database update
        updated_book_details = book_util.edit_book_details_subtract_1_from_jot_count(username, shelf_slug, book_slug)

        # perform the cache update
        cache_update.book_stat_updated(updated_book_details)

        return updated_book_details


    def lookup_does_book_exist_for_username_and_shelf_slug_and_book_slug(self,
                                                                         username,
                                                                         shelf_base_slug,
                                                                         book_base_slug):
        book_status = book_util.count_books_matching_username_shelf_and_slug(username, shelf_base_slug, book_base_slug)
        if book_status == 1: #messy but utilizes utility for adding numbers to slugs
            return False
        elif book_status >1:
            return True






