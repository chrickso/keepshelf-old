# -*- coding: utf-8 -*-

## UTILS CANNOT IMPORT OTHER UTILS

from flask import abort, session, url_for

from ksapp.extensions import db
from ksapp.etc.models.book_model import BookDB

from ksapp.etc.cache.setup import cache
from ksapp.etc.cache.keys import CacheKeys

from ksapp.etc.constraints import Constraints

from ksapp.etc.utils.time import TimeUtils

from ksapp.extensions import redis


cachekey = CacheKeys()
constraints = Constraints()

time_util = TimeUtils()


class BookUtils(object):

    def __init__(self):
        pass


    # # database record counts # #

    def count_books_matching_username_shelf_and_slug(self, username, shelf_slug, base_slug):
        """Get the count of how many books this user has with the same slug in the same shelf."""
        count = BookDB.query.filter_by(creator_username=username,
                                       shelf_slug=shelf_slug,
                                       base_slug=base_slug).count()
        return count + 1






    # # database lookups # #

    def lookup_book_details_by_id(self, jot_id):
        results = BookDB.query.get(jot_id)
        return results


    def lookup_book_details_by_username_and_shelf_slug_and_book_slug(self, username, shelf_slug, book_slug):
        book_details = BookDB.query.filter_by(creator_username=username,
                                              shelf_slug=shelf_slug,
                                              actual_slug=book_slug).first()
        return book_details


    def lookup_book_listing_full_by_username_and_shelf_slug(self, username, shelf_slug, sort='alpha'):
        results = BookDB.query.filter_by(creator_username=username,
                                         shelf_slug=shelf_slug) \
                              .filter(BookDB.attributes['disabled']=='0') \
                              .all()

        return results









    # # database modifications # #

    def edit_book_details_last_modified_to_right_now(self, username, shelf_slug, book_slug):
        book_details = self.lookup_book_details_by_username_and_shelf_slug_and_book_slug(username,
                                                                                 shelf_slug,
                                                                                 book_slug)
        book_details.last_modified = time_util.get_current_time()

        db.session.commit()

        return


    def db_mod_book_shelf_change(self, book_details, shelf_details):
        book_details = self.lookup_book_details_by_id(book_details.id)
        book_details.shelf_name = shelf_details.name
        book_details.shelf_slug = shelf_details.actual_slug
        db.session.commit()

        return book_details


    def edit_book_details_pin_by_id(self, book_id):
        book_details = self.lookup_book_details_by_id(book_id)
        book_details.attributes['pin'] = '1'
        db.session.commit()
        return book_details


    def edit_book_details_unpin_by_id(self, book_id):
        book_details = self.lookup_book_details_by_id(book_id)
        book_details.attributes['pin'] = '0'
        db.session.commit()
        return book_details


    def edit_book_details_add_1_to_jot_count(self, username, shelf_slug, book_slug):
        book_details = self.lookup_book_details_by_username_and_shelf_slug_and_book_slug(username, shelf_slug, book_slug)
        current_count = int(book_details.stats['jot_count'])
        new_count = current_count + 1
        book_details.stats['jot_count'] = str(new_count)
        db.session.commit()
        return book_details


    def edit_books_details_subtract_1_from_jot_count(self, username, shelf_slug, book_slug):
        book_details = lookup_book_details_by_username_and_shelf_slug_and_book_slug(username, shelf_slug, book_slug)
        current_count = int(book_details.stats['jot_count'])
        new_count = current_count - 1
        book_details.stats['jot_count'] = str(new_count)
        db.session.commit()
        return book_details










    # # cache gets # #

    def get_shelfs_book_listing_by_username_and_shelf_slug_sorted_alpha(self, username, shelf_slug, sort='alpha'):
        k = (cachekey.USERS_LISTING_OF_BOOKS_ON_SHELF_SORT_ALPHA % (username, shelf_slug)).encode('utf-8')
        v = cache.get(k)

        if v:
            if v == cachekey.NO_QUERY_RESULTS:
                return False
            else:
                return v
        else:
            v = BookDB.query.filter_by(creator_username=username,
                                       shelf_slug=shelf_slug) \
                            .filter(BookDB.attributes['disabled']=='0') \
                            .order_by(db.func.lower(BookDB.name)) \
                            .all()
            if v:
                cache.set(k, v)
            else:
                cache.set(k, cachekey.NO_QUERY_RESULTS)
        return v


    def get_book_details_by_username_and_shelf_slug_and_book_slug(self, username, shelf_slug, book_slug):
        k = (cachekey.SINGLE_BOOK_DETAILS_FROM_SLUG % (username, shelf_slug, book_slug)).encode('utf-8')
        v = cache.get(k)

        if v:
            if v == cachekey.NO_QUERY_RESULTS:
                abort(404)
            else:
                return v
        else:
            v = self.lookup_book_details_by_username_and_shelf_slug_and_book_slug(username,
                                                                                  shelf_slug,
                                                                                  book_slug)
            if v:
                cache.set(k, v)
            else:
                cache.set(k, cachekey.NO_QUERY_RESULTS)
                abort(404)

        return v



    def get_book_listing_full_by_username(self, username, sort='alpha'):
        k = (cachekey.USERS_FULL_BOOK_LISTING_SORT_ALPHA % username).encode('utf-8')
        v = cache.get(k)

        if v:
            if v == cachekey.NO_QUERY_RESULTS:
                return False
            else:
                return v
        else:
            v = BookDB.query.filter_by(creator_username=username) \
                            .filter(BookDB.attributes['disabled']=='0') \
                            .order_by(db.func.lower(BookDB.name)) \
                            .all()
            if v:
                cache.set(k, v)
            else:
                cache.set(k, cachekey.NO_QUERY_RESULTS)
        return v



    def get_book_listing_sidebar_by_username(self, username):
        k = (cachekey.USERS_SIDEBAR_BOOK_LISTING % username).encode('utf-8')
        v = cache.get(k)

        if v:
            if v == cachekey.NO_QUERY_RESULTS:
                return False
            else:
                return v
        else:
            v = BookDB.query.filter_by(creator_username=username) \
                            .filter(BookDB.attributes['disabled']=='0') \
                            .filter(BookDB.attributes['pin']=='1') \
                            .order_by(db.func.lower(BookDB.name)) \
                            .all()
            if v:
                cache.set(k, v)
            else:
                cache.set(k, cachekey.NO_QUERY_RESULTS)
        return v










    # # cache deletes # #

    def cache_delete_book_details_by_details(self, book_details):
        k = (cachekey.SINGLE_BOOK_DETAILS_FROM_SLUG % (book_details.creator_username,
                                                       book_details.shelf_slug,
                                                       book_details.actual_slug)).encode('utf-8')
        cache.delete(k)

        return book_details










    # # cache updates # #

    def cache_update_book_details_by_book_details(self, book_details):
        k = (cachekey.SINGLE_BOOK_DETAILS_FROM_SLUG % (book_details.creator_username,
                                                       book_details.shelf_slug,
                                                       book_details.actual_slug)).encode('utf-8')
        cache.set(k, book_details)

        return book_details



    def cache_update_book_listing_full_by_username(self, username, sort='alpha'):
        k = (cachekey.USERS_FULL_BOOK_LISTING_SORT_ALPHA % username).encode('utf-8')
        
        v = BookDB.query.filter_by(creator_username=username) \
                        .filter(BookDB.attributes['disabled']=='0') \
                        .order_by(db.func.lower(BookDB.name)) \
                        .all()
        if v:
            cache.set(k, v)
        else:
            cache.set(k, cachekey.NO_QUERY_RESULTS)
        return v



    def cache_update_book_listing_sidebar_by_username(self, username, sort='alpha'):
        k = (cachekey.USERS_SIDEBAR_BOOK_LISTING % username).encode('utf-8')
        
        v = BookDB.query.filter_by(creator_username=username) \
                            .filter(BookDB.attributes['disabled']=='0') \
                            .filter(BookDB.attributes['pin']=='1') \
                            .order_by(db.func.lower(BookDB.name)) \
                            .all()

        if v:
            cache.set(k, v)
        else:
            cache.set(k, cachekey.NO_QUERY_RESULTS)
        return v


    def cache_update_shelfs_book_listing_by_shelf_details(self, shelf_details, sort='alpha'):
        username = shelf_details.creator_username
        shelf_slug = shelf_details.actual_slug

        k = (cachekey.USERS_LISTING_OF_BOOKS_ON_SHELF_SORT_ALPHA % (username, shelf_slug)).encode('utf-8')
        
        v = BookDB.query.filter_by(creator_username=username,
                                   shelf_slug=shelf_slug) \
                        .filter(BookDB.attributes['disabled']=='0') \
                        .order_by(db.func.lower(BookDB.name)) \
                        .all()
        if v:
            cache.set(k, v)
        else:
            cache.set(k, cachekey.NO_QUERY_RESULTS)
        return v


    def cache_update_shelfs_book_listing_by_book_details(self, book_details, sort='alpha'):
        username = book_details.creator_username
        shelf_slug = book_details.shelf_slug

        k = (cachekey.USERS_LISTING_OF_BOOKS_ON_SHELF_SORT_ALPHA % (username, shelf_slug)).encode('utf-8')
        
        v = BookDB.query.filter_by(creator_username=username,
                                   shelf_slug=shelf_slug) \
                        .filter(BookDB.attributes['disabled']=='0') \
                        .order_by(db.func.lower(BookDB.name)) \
                        .all()
        if v:
            cache.set(k, v)
        else:
            cache.set(k, cachekey.NO_QUERY_RESULTS)
        return v

    


    # # redis updates

    def update_prev_and_next_book_in_shelf_slugs_sorted_set(self, username, shelf_slug):
        key = (cachekey.LISTING_OF_SHELFS_BOOK_SLUGS_SORT_ALPHA % (username, shelf_slug)).encode('utf-8')

        # get book listing
        shelfs_book_listing = self.get_shelfs_book_listing_by_username_and_shelf_slug_sorted_alpha(username, shelf_slug)

        # if results, add slugs to redis list with a score to keep list alphabetical
        if shelfs_book_listing:
            entry_score = 0.0
            for entry in shelfs_book_listing:
                entry_slug = (entry.actual_slug).encode('utf-8')

                entry_score += 1.0
                redis.zadd(key, entry_slug, entry_score)

        return


    def delete_prev_and_next_book_in_shelf_slugs_sorted_set(self, username, shelf_slug):
        key = (cachekey.LISTING_OF_SHELFS_BOOK_SLUGS_SORT_ALPHA % (username, shelf_slug)).encode('utf-8')
        redis.delete(key)
        return



    



    



    
            
