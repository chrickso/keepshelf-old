# -*- coding: utf-8 -*-

from flask import (Blueprint, current_app, request, render_template,
                   g, redirect, url_for, flash, session, abort)

from ksapp.mods.book.functions import BookFunctions
from ksapp.mods.shelf.utils import ShelfUtils
from ksapp.mods.book.utils import BookUtils
from ksapp.etc.utils.strings import StringFunctions
from ksapp.mods.jot.functions import JotFunctions
from ksapp.mods.jot.utils import JotUtils

from ksapp.etc.constraints import Constraints

from ksapp.etc.operations import RenameOperations
from ksapp.etc.operations import DeleteOperations


from ksapp.etc.utils.slug import slugify

from ksapp.mods.user.sidebar.functions import render_sidebar_template

from ksapp.etc.form_processing import BookForms, JotForms


bookBP = Blueprint('bookBP', __name__)

shelf_util = ShelfUtils()
book_func = BookFunctions()
book_util = BookUtils()
string_func = StringFunctions()
jot_func = JotFunctions()
jot_util = JotUtils()
constraints = Constraints()
rename_op = RenameOperations()
del_op = DeleteOperations()
book_form_proc = BookForms()
jot_form_proc = JotForms()




@bookBP.route('/u/<user_username>/books/', methods=['GET', 'POST'])
def listing(user_username):
    up_level_url = url_for('userBP.profile', user_username=user_username)

    if request.method == 'POST':

        if session.get('user_username') != user_username:
            flash(u'You are not authorized to POST here.', u'notice')
            return redirect(url_for('bookBP.details',
                                    user_username=user_username))

        process_pin_updates = book_form_proc.process_book_listing_form_submission_to_pin_and_unpin()

    book_list = book_util.get_book_listing_full_by_username(user_username)

    return render_sidebar_template('mods/book/listing.html',
                                    up_level_url=up_level_url,
                                    user_username=user_username,
                                    book_list=book_list)



@bookBP.route('/u/<user_username>/book/', methods=['GET', 'POST'])
def new(user_username):
    next = url_for('bookBP.new', user_username=user_username)
    up_level_url = url_for('bookBP.listing', user_username=user_username)

    if not session.get('user_username'):
        flash(u'You must be logged in to create.', u'notice')
        return redirect(url_for('userBP.login', next=next))

    post_inputs={}
    post_errors={}

    if request.method == 'POST':

        post_inputs, post_errors, success_url = book_form_proc.process_form_new_book()

        if success_url:
            return redirect(success_url)

    users_shelf_list = shelf_util.get_shelf_listing_full_by_username(user_username)

    return render_sidebar_template('mods/book/new.html',
                                   next=next,
                                   up_level_url=up_level_url,
                                   post_inputs=post_inputs,
                                   post_errors=post_errors,
                                   users_shelf_list=users_shelf_list)



@bookBP.route('/u/<user_username>/shelf/<shelf_slug>/book/<book_slug>', methods=['GET', 'POST'])
def details(user_username, shelf_slug, book_slug):
    up_level_url = url_for('shelfBP.details', user_username=user_username, shelf_slug=shelf_slug)

    # verify this is an existing user or redirect to frontpage with flash msg

    # get pin'd data

    book_details = book_util.get_book_details_by_username_and_shelf_slug_and_book_slug(user_username,
                                                      shelf_slug,
                                                      book_slug)

    books_jot_listing = jot_util.get_jot_listing_by_book_slug(user_username,
                                                              shelf_slug,
                                                              book_slug)

    prev_book_in_shelf_slug, \
    next_book_in_shelf_slug = book_func.get_prev_and_next_book_in_shelf_slugs(user_username,
                                                                              shelf_slug,
                                                                              book_slug)


    if request.method == 'POST':        

        processed_form_redirect = jot_form_proc.process_form_new_or_edit_jot(user_username,
                                                                        shelf_slug,
                                                                        book_slug)

        if processed_form_redirect:
            return redirect(processed_form_redirect)

    # get full shelf & book listing for sub-header dropdowns
    if session.get('user_username'):
        shelf_listing = shelf_util.get_shelf_listing_full_by_username(session['user_username'])
        book_listing = book_util.get_book_listing_full_by_username(session['user_username'])
    else:
        shelf_listing = None
        book_listing = None


    return render_sidebar_template('mods/book/details.html',
                            # shelf_listing=shelf_listing,
                            up_level_url=up_level_url,
                            user_username=user_username,
                            book_details=book_details,
                            books_jot_listing=books_jot_listing,
                            prev_book_in_shelf_slug=prev_book_in_shelf_slug,
                            next_book_in_shelf_slug=next_book_in_shelf_slug,
                            shelf_listing=shelf_listing,
                            book_listing=book_listing,
                            )



@bookBP.route('/u/<user_username>/shelf/<shelf_slug>/book/<book_slug>/rename/', methods=['GET', 'POST'])
def rename(user_username, shelf_slug, book_slug):
    next = url_for('bookBP.rename', user_username=user_username, shelf_slug=shelf_slug, book_slug=book_slug)
    up_level_url = url_for('bookBP.details', user_username=user_username, shelf_slug=shelf_slug, book_slug=book_slug)

    if session.get('user_username') != user_username:
        flash(u'You are not the owner of this shelf.', u'notice')
        return redirect(url_for('shelfBP.details',
                                user_username=user_username,
                                shelf_slug=shelf_slug))
    else:

        original_book_details = book_util.get_book_details_by_username_and_shelf_slug_and_book_slug(user_username, shelf_slug, book_slug)

        post_inputs={}
        post_errors={}

        post_inputs['book_name'] = original_book_details.name

        if request.method == 'POST':

            book_name = string_func.removeEndSpace(request.form.get('book_name'))


            if book_name != original_book_details.name:
                new_book_name = book_name
            else:
                new_book_name = None


            if not new_book_name:
                flash(u'A new book name was not provided.', u'notice')
            else:
                if new_book_name:
                    post_inputs['book_name'] = new_book_name
                else:
                    post_errors['book_name'] = u'You must provide a book name.'


                # validate form

                # do inputs fall within length range?
                if new_book_name and len(new_book_name) > constraints.BOOK_NAME_MAX_CHARS:
                    post_errors['book_name'] = u'That name is too long. Max: %s characters.' % constraints.BOOK_NAME_MAX_CHARS

                # if no errors, generate book name slug
                if not post_errors.get('book_name'):

                    base_slug = slugify(new_book_name)

                    actual_slug = base_slug

                    duplicate_slug_count = book_util.count_books_matching_username_shelf_and_slug(user_username, shelf_slug, base_slug)

                    if duplicate_slug_count > 1:
                        actual_slug = base_slug + '-%s' % duplicate_slug_count
                        book_name = book_name + '-%s' % duplicate_slug_count


                    # if everything passes, insert new book in BookDB

                    updated_book_details = rename_op.modify_book_name(
                                                                        original_book_details,
                                                                        new_book_name,
                                                                        base_slug,
                                                                        actual_slug)

                    if not updated_book_details:
                        flash(u'Something went wrong while renaming your book.', u'error')
                    else:
                        return redirect(url_for('bookBP.details',
                                                user_username=session['user_username'],
                                                shelf_slug=updated_book_details.shelf_slug,
                                                book_slug=updated_book_details.actual_slug))
    return render_sidebar_template('mods/book/rename.html',
                           next=next,
                           up_level_url=up_level_url,
                           post_inputs=post_inputs,
                           post_errors=post_errors,
                           book_details=original_book_details
        )



@bookBP.route('/u/<user_username>/shelf/<shelf_slug>/book/<book_slug>/delete/', methods=['GET', 'POST'])
def delete(user_username, shelf_slug, book_slug):
    up_level_url = url_for('bookBP.details', user_username=user_username, shelf_slug=shelf_slug, book_slug=book_slug)

    if session.get('user_username') != user_username:
        flash(u'You are not the owner of this book.', u'notice')
        return redirect(url_for('bookBP.details',
                                user_username=user_username,
                                shelf_slug=shelf_slug,
                                book_slug=book_slug))
    else:

        book_details = book_util.get_book_details_by_username_and_shelf_slug_and_book_slug(user_username,
                                                          shelf_slug,
                                                          book_slug)


        if request.method == 'POST':

            del_op.delete_book_and_jots_by_book_details(book_details)

            return redirect(url_for('shelfBP.details',
                                user_username=user_username,
                                shelf_slug=shelf_slug))

        return render_sidebar_template('mods/book/delete.html',
                                        up_level_url=up_level_url,
                                        book_details=book_details)



@bookBP.route('/u/<user_username>/shelf/<shelf_slug>/book/<book_slug>/change_shelf/', methods=['GET', 'POST'])
def change_shelf(user_username, shelf_slug, book_slug):
    next = url_for('bookBP.change_shelf', user_username=user_username, shelf_slug=shelf_slug, book_slug=book_slug)
    up_level_url = url_for('bookBP.details', user_username=user_username, shelf_slug=shelf_slug, book_slug=book_slug)

    if not session.get('user_username'):
        flash(u'You must be logged in to create.', u'notice')
        return redirect(url_for('userBP.login', next=next))

    original_book_details = book_util.get_book_details_by_username_and_shelf_slug_and_book_slug(user_username, shelf_slug, book_slug)

    post_inputs={}
    post_errors={}

    post_inputs['shelf'] = original_book_details.shelf_slug

    if request.method == 'POST':

        post_inputs, post_errors, success_url = book_form_proc.process_form_change_book_shelf(original_book_details)

        if success_url:
            return redirect(success_url)

    users_shelf_list = shelf_util.get_shelf_listing_full_by_username(user_username)

    return render_sidebar_template('mods/book/change_shelf.html',
                                   next=next,
                                   up_level_url=up_level_url,
                                   post_inputs=post_inputs,
                                   post_errors=post_errors,
                                   users_shelf_list=users_shelf_list,
                                   book_details=original_book_details)