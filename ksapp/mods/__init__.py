# -*- coding: utf-8 -*-

from ksapp.mods.book.controller import bookBP

from ksapp.mods.frontpage.controller import frontpageBP

from ksapp.mods.jot.controller import jotBP

from ksapp.mods.shelf.controller import shelfBP

from ksapp.mods.user.controller import userBP