# -*- coding: utf-8 -*-

import datetime

from flask import session

from ksapp.extensions import db
from ksapp.etc.models.shelf_model import ShelfDB

# # from pbapp.mods.etc.cache.setup import cache
from ksapp.etc.cache.keys import CacheKeys

# from pbapp.mods.etc.utils.post_utils import PostUtils

from ksapp.extensions import redis


cachekey = CacheKeys()
# post_util = PostUtils()




class FrontpageFunctions(object):

    def __init__(self):
        pass


    def generate_global_activity_stream_list(self, left_range, right_range):

        list_of_activity_ids = redis.lrange(cachekey.GLOBAL_ACTIVITY_STREAM_LIST_OF_ACTIVITY_IDS, 0, 200)

        global_activity_stream = []

        for id in list_of_activity_ids:
            activity_message, activity_time = redis.hmget((cachekey.ACTIVITY_DETAILS % id), 'message', 'time')

            entry = []

            entry.append(activity_message)
            entry.append(datetime.datetime.strptime(activity_time, '%Y-%m-%d %H:%M:%S.%f'))

            global_activity_stream.append(entry)

        return global_activity_stream





    
