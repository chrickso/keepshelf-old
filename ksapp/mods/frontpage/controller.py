# -*- coding: utf-8 -*-

from flask import (Blueprint, current_app, request, render_template,
                   g, redirect, url_for, flash, session, abort)

from ksapp.mods.user.sidebar.functions import render_sidebar_template

from ksapp.extensions import redis

from ksapp.etc.cache.keys import CacheKeys

from ksapp.mods.frontpage.functions import FrontpageFunctions


cachekey = CacheKeys()
fp_func = FrontpageFunctions()

# from pbapp.mods.home.functions import HomeFunctions


frontpageBP = Blueprint('frontpageBP', __name__)


# home_func = HomeFunctions()


@frontpageBP.route('/')
def index():

    # global_activity_stream = fp_func.generate_global_activity_stream_list(0, 200)

    return render_sidebar_template('mods/frontpage/index.html',
                                    # global_activity_stream=global_activity_stream,
                                    )

