# -*- coding: utf-8 -*-

## UTILS CANNOT IMPORT OTHER UTILS

from flask import session

from ksapp.extensions import db
from ksapp.etc.models.jot_model import JotDB

from ksapp.etc.cache.setup import cache
from ksapp.etc.cache.keys import CacheKeys
from ksapp.etc.cache.utils import CacheUtils

from ksapp.etc.utils.time import TimeUtils


cachekey = CacheKeys()
cache_util = CacheUtils()
time_utils = TimeUtils()



class JotUtils(object):

    def __init__(self):
        pass



    # # database lookups # #

    def lookup_jot_details_by_id(self, jot_id):
        results = JotDB.query.get(jot_id)
        return results


    def lookup_jot_listing_by_book_slug(self, username, shelf_slug, book_slug, sort="newest_first"):
        results = JotDB.query.filter_by(creator_username=username,
                                        shelf_slug=shelf_slug,
                                        book_slug=book_slug) \
                            .order_by(JotDB.time_created.desc()) \
                            .all()
        return results





    # # database modifications # #

    def db_modify_jots_shelf_slug_and_name(self, jot_details, new_shelf_slug, new_shelf_name):
        jot_details = self.lookup_jot_details_by_id(jot_details.id)
        jot_details.shelf_slug = new_shelf_slug
        jot_details.shelf_name = new_shelf_name
        db.session.commit()
        return jot_details

    def edit_existing_jot_details(self, original_jot_details, new_jot_details):
        jot_details = self.lookup_jot_details_by_id(original_jot_details.id)
        jot_details = new_jot_details

        jot_details.last_modified = time_utils.get_current_time()

        db.session.add(jot_details)

        db.session.commit()

        return True



    def delete_jot_from_db(self, jot_id, book_modified_update=None):
        """Removes jot record from JotDB and deletes jot's cache'd details."""
        jot_details = JotDB.query.get(jot_id)
        db.session.delete(jot_details)

        db.session.commit()

        return jot_details







    # # cache gets # #

    def get_jot_details_by_id(self, jot_id):
        k = (cachekey.SINGLE_JOT_DETAILS_FROM_ID % jot_id).encode('utf-8')
        v = cache.get(k)

        if v:
            v = cache_util.check_cache_for_no_query_results(v)

        else:
            v = self.lookup_jot_details_by_id(jot_id)
            cache_util.cache_set_results_or_no_results(k, v)

        return v




    def get_jot_listing_by_book_slug(self, username, shelf_slug, book_slug, sort="newest_first"):
        k = (cachekey.USERS_LISTING_OF_JOTS_IN_BOOK_SORT_NEWEST_FIRST % (username, shelf_slug, book_slug)).encode('utf-8')
        v = cache.get(k)

        if v:
            v = cache_util.check_cache_for_no_query_results(v)

        else:
            v = self.lookup_jot_listing_by_book_slug(username, shelf_slug, book_slug)
            cache_util.cache_set_results_or_no_results(k, v)

        return v







    # # cache deletes # #

    def cache_delete_jot_listing_by_book_slug(self, username, shelf_slug, book_slug, sort="newest_first"):
        k = (cachekey.USERS_LISTING_OF_JOTS_IN_BOOK_SORT_NEWEST_FIRST % (username, shelf_slug, book_slug)).encode('utf-8')
        cache.delete(k)

        return


    def cache_delete_jot_details_by_id(self, jot_id):
        k = (cachekey.SINGLE_JOT_DETAILS_FROM_ID % jot_id).encode('utf-8')
        cache.delete(k)

        return








    # # cache updates # #

    def cache_update_jot_listing_by_book_slug(self, username, shelf_slug, book_slug, sort="newest_first"):
        k = (cachekey.USERS_LISTING_OF_JOTS_IN_BOOK_SORT_NEWEST_FIRST % (username, shelf_slug, book_slug)).encode('utf-8')
        v = self.lookup_jot_listing_by_book_slug(username, shelf_slug, book_slug)

        cache_util.cache_set_results_or_no_results(k, v)

        return v




    def cache_update_jot_details(self, jot_details):
        k = (cachekey.SINGLE_JOT_DETAILS_FROM_ID % jot_details.id).encode('utf-8')
        cache.set(k, jot_details)

        return True




    # # helper utilities # #

    def create_jot_attachments_dict_from_list(self, attachments_list):
        # create & process attachments dictionary
        attachment_dict = {}

        #if any attachments where supplied with the jot
        if attachments_list != None:
            attachment_count = 1

            # process the list of attachments sent from the new jot form
            for attachment in attachments_list:

                # find the attachments location in the list
                attachment_list_location = attachment_count - 1

                # add it to the dictionary with the key being the current count
                attachment_dict[str(attachment_count)] = '%s' % attachments_list[attachment_list_location]

                # increment the count so the next pass gets the next list entry
                attachment_count += 1

        return attachment_dict


    def compare_jot_attachments_dict_and_return_change_count(self, original_dict, new_dict):
        change_count = 0

        if len(original_dict) == len(new_dict):

            for key, value in new_dict.iteritems():
                if original_dict[key] != value:
                    change_count += 1
        else:
            # if length changed, something is different and for loop won't catch if len(new_dict) == 0
            change_count = 1

        return change_count

