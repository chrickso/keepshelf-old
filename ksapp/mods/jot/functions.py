# -*- coding: utf-8 -*-

from flask import flash, request, session, url_for

from ksapp.extensions import db
from ksapp.etc.models.jot_model import JotDB

from ksapp.etc.cache.updates import CacheUpdates

from ksapp.mods.book.functions import BookFunctions

from ksapp.mods.shelf.utils import ShelfUtils
from ksapp.mods.book.utils import BookUtils
from ksapp.mods.jot.utils import JotUtils

from ksapp.etc.activity.updates import ActivityUpdates

from ksapp.etc.utils.strings import StringFunctions

# # # from pbapp.mods.etc.cache.setup import cache
# # from pbapp.mods.etc.cache.keys import CacheKeys

# # from pbapp.mods.etc.utils.post_utils import PostUtils

# # from pbapp.extensions import redis


# # cachekey = CacheKeys()
# # post_util = PostUtils()

cache_update = CacheUpdates()
shelf_util = ShelfUtils()
book_util = BookUtils()
jot_util = JotUtils()
book_func = BookFunctions()
activity_update = ActivityUpdates()
string_func = StringFunctions()



class JotFunctions(object):

    def __init__(self):
        pass


    # # create # #

    def insert_new_jot_into_db(self,
                               shelf_slug,
                               book_slug,
                               message,
                               attachments,
                               user_username,
                               user_id
                               ):

        if session.get('default_share_level') == 'private':
            share_level = 0
        else:
            share_level = 1

        if not user_username:
            user_username=session['user_username']

        if not user_id:
            user_username=session['user_id']

        book_details = book_util.get_book_details_by_username_and_shelf_slug_and_book_slug(user_username, shelf_slug, book_slug)

        shelf_name = book_details.shelf_name
        book_name = book_details.name

        # create & process attachments dictionary
        attachment_dict = jot_util.create_jot_attachments_dict_from_list(attachments)

        if not user_username:
            user_username=session['user_username']
        if not user_id:
            user_username=session['user_id']

        # create the new_jot object and attach data
        new_jot = JotDB(message=message,
                        creator_id=user_id,
                        creator_username=user_username,
                        shelf_name=shelf_name,
                        shelf_slug=shelf_slug,
                        book_name=book_name,
                        book_slug=book_slug,
                        share_level=share_level,
                        attachments=attachment_dict)

        db.session.add(new_jot)

        db.session.commit()

        # k = (cachekey.BOARD_LIST_QUERY %(place_url, type)).lower().encode('utf-8')
        # cache.delete(k)

        # k = (cachekey.BOARD_URL_EXISTS_CHECK % (place_url, board_url)).lower().encode('utf-8')
        # cache.set(k, True)

        # k = (cachekey.BOARD_NAME_EXISTS_CHECK % (place_url, string_func.sanitize_board_name(board_name))).lower().encode('utf-8')
        # cache.set(k, True)

        # # update the place_details cache because it has a new board_list entry
        # k = (cachekey.SINGLE_PLACE_QUERY % place_url).encode('utf-8')
        # cache.set(k, place_details)

        # #cache_update.new_board_cache_update(place_url, board_url, board_name)

        cache_update.user_creates_new_jot(new_jot)
        activity_update.user_creates_a_jot(new_jot)

        # add one to the shelf's book_count
        book_details = book_func.edit_book_details_add_1_to_jot_count_and_update_caches(user_username, shelf_slug, book_slug)

        return True




    # # jot modifications # #

    def edit_jot_with_cache_updates(self, original_jot_details, new_jot_details):
        db_update_status = jot_util.edit_existing_jot_details(original_jot_details, new_jot_details)
        cache_update_status = cache_update.jot_edited(new_jot_details)

        return True



    def update_all_jots_with_old_shelf_name_and_slug_with_new_shelf_name_and_slug(self,
                                                                                  original_shelf_details,
                                                                                  new_shelf_details):
        # get a list of all jots in every book on the shelf that was updated
        all_records = JotDB.query.filter_by(creator_username=original_shelf_details.creator_username,
                                            shelf_slug=original_shelf_details.actual_slug).all()
        # for every record found
        for record in all_records:

            # delete the cached details (they are now outdated)
            jot_util.cache_delete_jot_details_by_id(record.id)

            # update the database with new information
            record.shelf_slug = new_shelf_details.actual_slug
            record.shelf_name = new_shelf_details.name

            # set the new book details in cache
            jot_util.cache_update_jot_details(record)

            # add the updated record to the session
            db.session.add(record)

        # commit all changes to updated records
        db.session.commit()

        return True



    def update_all_jots_with_old_book_name_and_slug_with_new_book_name_and_slug(self,
                                                                                original_book_details,
                                                                                new_book_details):
        # get a list of all jots in the book that was updated
        all_records = JotDB.query.filter_by(creator_username=original_book_details.creator_username,
                                            shelf_slug=original_book_details.shelf_slug,
                                            book_slug=original_book_details.actual_slug).all()
        # for every record found
        for record in all_records:

            # delete the cached details (they are now outdated)
            jot_util.cache_delete_jot_details_by_id(record.id)

            # update the database with new information
            record.book_slug = new_book_details.actual_slug
            record.book_name = new_book_details.name

            # set the new book details in cache
            jot_util.cache_update_jot_details(record)

            # add the updated record to the session
            db.session.add(record)

        # commit all changes to updated records
        db.session.commit()

        return True



    # # delete # #

    def delete_jot_from_db_with_cache_updates(self, jot_id, book_modified_update=None):
        """Removes jot record from JotDB and deletes jot's cache'd details."""
        deleted_jot_details = jot_util.delete_jot_from_db(jot_id)

        jot_util.cache_delete_jot_details_by_id(deleted_jot_details.id)

        cache_update.jot_deleted_from_db(deleted_jot_details, book_modified_update)

        return True