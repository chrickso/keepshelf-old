# -*- coding: utf-8 -*-

from flask import (Blueprint, current_app, request, render_template,
                   g, redirect, url_for, flash, session, abort)

from ksapp.mods.jot.utils import JotUtils
from ksapp.mods.jot.functions import JotFunctions
# from ksapp.mods.book.functions import BookFunctions
# from ksapp.mods.book.utils import BookUtils
# from ksapp.mods.shelf.utils import ShelfUtils
from ksapp.etc.utils.strings import StringFunctions
# from ksapp.etc.constraints import Constraints

# from ksapp.etc.utils.slug import slugify

from ksapp.mods.user.sidebar.functions import render_sidebar_template

from ksapp.etc.form_processing import JotForms


jotBP = Blueprint('jotBP', __name__)


jot_util = JotUtils()
jot_func = JotFunctions()
# book_func = BookFunctions()
# book_util = BookUtils()
# shelf_util = ShelfUtils()
string_func = StringFunctions()
# constraints = Constraints()
jot_form_proc = JotForms()



@jotBP.route('/u/<user_username>/<shelf_slug>/book/<book_slug>/<int:jot_id>')
def details(user_username, shelf_slug, book_slug, jot_id):
    up_level_url = url_for('bookBP.details', user_username=user_username, shelf_slug=shelf_slug, book_slug=book_slug)

    jot_details = jot_util.get_jot_details_by_id(jot_id)

    # if jot_details.share_level == 0, render 'jot is private' template

    return render_sidebar_template('mods/jot/details.html',
                                   # shelf_listing=shelf_listing,
                                   up_level_url=up_level_url,
                                   jot_details=jot_details,
                                   )



@jotBP.route('/u/<user_username>/<shelf_slug>/<book_slug>/<int:jot_id>/delete/', methods=['GET', 'POST'])
def delete(user_username, shelf_slug, book_slug, jot_id):
    up_level_url = url_for('jotBP.details', user_username=user_username, shelf_slug=shelf_slug, book_slug=book_slug, jot_id=jot_id)

    if session.get('user_username') != user_username:
        flash(u'You are not the owner of this jot.', u'notice')
        return redirect(url_for('bookBP.details',
                                user_username=user_username,
                                shelf_slug=shelf_slug,
                                book_slug=book_slug))
    else:

        jot_details = jot_util.get_jot_details_by_id(jot_id)

        if jot_details == None or jot_details.creator_username != user_username:
            flash(u'That jot ID no longer exists or is no longer yours.', u'notice')
            return redirect(url_for('bookBP.details',
                                user_username=user_username,
                                shelf_slug=shelf_slug,
                                book_slug=book_slug))

        if request.method == 'POST':

            jot_func.delete_jot_from_db_with_cache_updates(jot_id, book_modified_update=True)

            return redirect(url_for('bookBP.details',
                                user_username=user_username,
                                shelf_slug=shelf_slug,
                                book_slug=book_slug))

        return render_sidebar_template('mods/jot/delete.html',
                                        up_level_url=up_level_url,
                                        jot_details=jot_details)



@jotBP.route('/u/<user_username>/<shelf_slug>/<book_slug>/<int:jot_id>/edit/', methods=['GET', 'POST'])
def edit(user_username, shelf_slug, book_slug, jot_id):
    up_level_url = url_for('jotBP.details', user_username=user_username, shelf_slug=shelf_slug, book_slug=book_slug, jot_id=jot_id)

    if session.get('user_username') != user_username:
        flash(u'You are not the owner of this jot.', u'notice')
        return redirect(url_for('bookBP.details',
                                user_username=user_username,
                                shelf_slug=shelf_slug,
                                book_slug=book_slug))
    else:

        jot_details = jot_util.get_jot_details_by_id(jot_id)

        if jot_details == None or jot_details.creator_username != user_username:
            flash(u'That jot ID no longer exists or is no longer yours.', u'notice')
            return redirect(url_for('bookBP.details',
                                user_username=user_username,
                                shelf_slug=shelf_slug,
                                book_slug=book_slug))

        if request.method == 'POST':

            processed_form_redirect = jot_form_proc.process_form_new_or_edit_jot(user_username,
                                                                        shelf_slug,
                                                                        book_slug,
                                                                        jot_details)

            if processed_form_redirect:
                return redirect(processed_form_redirect)

        return render_sidebar_template('mods/jot/edit.html',
                                        up_level_url=up_level_url,
                                        jot_details=jot_details)

