# -*- coding: utf-8 -*-

## UTILS CANNOT IMPORT OTHER UTILS

from flask import abort, session

from ksapp.extensions import db
from ksapp.etc.models.shelf_model import ShelfDB

from ksapp.etc.cache.setup import cache
from ksapp.etc.cache.keys import CacheKeys

from ksapp.etc.constraints import Constraints


cachekey = CacheKeys()
constraints = Constraints()



class ShelfUtils(object):

    def __init__(self):
        pass


    # # database record counts # #

    def count_shelves_matching_username_and_slug(self, username, base_slug):
        """Get the count of how many shelves this user has with the same slug."""
        count = ShelfDB.query.filter_by(creator_username=username,
                                        base_slug=base_slug) \
                              .filter(ShelfDB.attributes['disabled']=='0') \
                              .count()
        return count + 1


    # # database inserts # #

    def db_insert_new_shelf(self,
                            shelf_name,
                            base_slug,
                            actual_slug,
                            pin_state,
                            visibility,
                            user_id=None,
                            username=None):
        # if session.get('default_share_level') == 'private':
        #     share_level = 0
        # else:
        #     share_level = 1

        if user_id == None:
            user_id = session['user_id']

        if username == None:
            username = session['user_username']

        new_shelf_details = ShelfDB(name=shelf_name,
                                    creator_id=user_id,
                                    creator_username=username,
                                    base_slug=base_slug,
                                    actual_slug=actual_slug,
                                    attributes={ 'pin' : pin_state,
                                                 'visibility' : visibility,
                                                 'disabled' : '0', }
                                    )
        db.session.add(new_shelf_details)

        db.session.commit()

        return new_shelf_details

    


    # # database lookups # #

    def lookup_shelf_details_by_id(self, shelf_id):
        results = ShelfDB.query.get(shelf_id)
        return results


    def lookup_shelf_details_by_username_and_shelf_slug(self, username, shelf_slug):
        result = ShelfDB.query.filter_by(creator_username=username,
                                         actual_slug=shelf_slug) \
                              .first()
        return result




    # # database modifications # #

    def edit_shelf_details_pin_by_id(self, shelf_id):
        shelf_details = self.lookup_shelf_details_by_id(shelf_id)
        shelf_details.attributes['pin'] = '1'
        db.session.commit()
        return shelf_details


    def edit_shelf_details_unpin_by_id(self, shelf_id):
        shelf_details = self.lookup_shelf_details_by_id(shelf_id)
        shelf_details.attributes['pin'] = '0'
        db.session.commit()
        return shelf_details


    def edit_shelf_details_pin_and_visibility(self, shelf_id, new_pin_status=None, new_visibility=None):
        shelf_details = self.lookup_shelf_details_by_id(shelf_id)
        if new_pin_status:
            shelf_details.attributes['pin'] = '%s' % new_pin_status
        if new_visibility:
            shelf_details.attributes['visibility'] = new_visibility
        db.session.commit()
        return shelf_details


    def edit_shelf_details_add_1_to_book_count(self, username, shelf_slug):
        shelf_details = self.lookup_shelf_details_by_username_and_shelf_slug(username, shelf_slug)
        current_count = int(shelf_details.stats['book_count'])
        new_count = current_count + 1
        shelf_details.stats['book_count'] = str(new_count)
        db.session.commit()
        return shelf_details


    def edit_shelf_details_subtract_1_from_book_count(self, username, shelf_slug):
        shelf_details = self.lookup_shelf_details_by_username_and_shelf_slug(username, shelf_slug)
        current_count = int(shelf_details.stats['book_count'])
        new_count = current_count - 1
        shelf_details.stats['book_count'] = str(new_count)
        db.session.commit()
        return shelf_details



    # # cache gets # #

    def get_shelf_details_by_username_and_shelf_slug(self, username, slug):
        k = (cachekey.SINGLE_SHELF_DETAILS_FROM_SLUG % (username, slug)).encode('utf-8')
        v = cache.get(k)

        if v:
            if v == cachekey.NO_QUERY_RESULTS:
                abort(404)
            else:
                return v
        else:
            v = ShelfDB.query.filter_by(creator_username=username,
                                        actual_slug=slug) \
                              .filter(ShelfDB.attributes['disabled']=='0') \
                              .first()

            if v:
                cache.set(k, v)
            else:
                cache.set(k, cachekey.NO_QUERY_RESULTS)
                abort(404)

        return v


    def get_shelf_listing_full_by_username(self, username, sort='alpha'):
        k = (cachekey.USERS_FULL_SHELF_LISTING_SORT_ALPHA % username).encode('utf-8')
        v = cache.get(k)

        if v:
            if v == cachekey.NO_QUERY_RESULTS:
                return False
            else:
                return v
        else:
            v = ShelfDB.query.filter_by(creator_username=username) \
                              .filter(ShelfDB.attributes['disabled']=='0') \
                              .order_by(db.func.lower(ShelfDB.name)) \
                              .all()
            if v:
                cache.set(k, v)
            else:
                cache.set(k, cachekey.NO_QUERY_RESULTS)
        return v


    def get_shelf_listing_sidebar_by_username(self, username):
        k = (cachekey.USERS_SIDEBAR_SHELF_LISTING % username).encode('utf-8')
        v = cache.get(k)

        if v:
            if v == cachekey.NO_QUERY_RESULTS:
                return False
            else:
                return v
        else:
            v = ShelfDB.query.filter_by(creator_username=username) \
                             .filter(ShelfDB.attributes['disabled']=='0') \
                             .filter(ShelfDB.attributes['pin']=='1') \
                             .order_by(db.func.lower(ShelfDB.name)) \
                             .all()
            if v:
                cache.set(k, v)
            else:
                cache.set(k, cachekey.NO_QUERY_RESULTS)
        return v


    







    # # cache deletes # #

    def cache_delete_shelf_details_by_username_and_slug(self, username, slug):
        k = (cachekey.SINGLE_SHELF_DETAILS_FROM_SLUG % (username, slug)).encode('utf-8')
        cache.delete(k)
        return


    def cache_delete_shelf_details_by_shelf_details(self, shelf_details):
        k = (cachekey.SINGLE_SHELF_DETAILS_FROM_SLUG % (shelf_details.creator_username, shelf_details.actual_slug)).encode('utf-8')
        cache.delete(k)
        return


    def cache_delete_shelfs_book_listing_by_username_and_shelf_slug(self, username, shelf_slug):
        k = (cachekey.USERS_LISTING_OF_BOOKS_ON_SHELF_SORT_ALPHA % (username, shelf_slug)).encode('utf-8')
        cache.delete(k)
        return




    # # cache updates # #

    def cache_update_shelf_details_by_shelf_details(self, shelf_details):
        username = shelf_details.creator_username
        shelf_slug = shelf_details.actual_slug

        k = (cachekey.SINGLE_SHELF_DETAILS_FROM_SLUG % (username, shelf_slug)).encode('utf-8')
        cache.set(k, shelf_details)
        return


    def cache_update_shelf_listing_full_by_username(self, username, sort='alpha'):
        k = (cachekey.USERS_FULL_SHELF_LISTING_SORT_ALPHA % username).encode('utf-8')
        
        v = ShelfDB.query.filter_by(creator_username=username) \
                         .filter(ShelfDB.attributes['disabled']=='0') \
                         .order_by(db.func.lower(ShelfDB.name)) \
                         .all()
        if v:
            cache.set(k, v)
        else:
            cache.set(k, cachekey.NO_QUERY_RESULTS)
        return v


    def cache_update_shelf_listing_sidebar_by_username(self, username, sort='alpha'):
        k = (cachekey.USERS_SIDEBAR_SHELF_LISTING % username).encode('utf-8')
        
        v = ShelfDB.query.filter_by(creator_username=username) \
                         .filter(ShelfDB.attributes['disabled']=='0') \
                         .filter(ShelfDB.attributes['pin']=='1') \
                         .order_by(db.func.lower(ShelfDB.name)) \
                         .all()
        if v:
            cache.set(k, v)
        else:
            cache.set(k, cachekey.NO_QUERY_RESULTS)
        return v



    


    



    


    



    



    



    



    



    


    


    