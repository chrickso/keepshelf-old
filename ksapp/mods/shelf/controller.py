# -*- coding: utf-8 -*-

from flask import (Blueprint, current_app, request, render_template,
                   g, redirect, url_for, flash, session, abort)

from ksapp.mods.shelf.functions import ShelfFunctions
from ksapp.mods.shelf.utils import ShelfUtils
from ksapp.mods.book.functions import BookFunctions
from ksapp.mods.book.utils import BookUtils
from ksapp.etc.utils.strings import StringFunctions
from ksapp.etc.constraints import Constraints
from ksapp.etc.messages import Messages
from ksapp.etc.utils.user import UserUtils

from ksapp.etc.utils.slug import slugify

from ksapp.mods.user.sidebar.functions import render_sidebar_template

from ksapp.etc.operations import DeleteOperations

from ksapp.etc.form_processing import ShelfForms
from ksapp.etc.form_processing import BookForms

import copy


shelfBP = Blueprint('shelfBP', __name__)


shelf_func = ShelfFunctions()
shelf_util = ShelfUtils()
book_func = BookFunctions()
book_util = BookUtils()
string_func = StringFunctions()
constraints = Constraints()
messages = Messages()
del_op = DeleteOperations()
shelf_form_proc = ShelfForms()
book_form_proc = BookForms()
user_util = UserUtils()

# remove from controller once the 'process_shelf_rename' is done as a function
from ksapp.etc.operations import RenameOperations
rename_op = RenameOperations()



@shelfBP.route('/u/<user_username>/shelf/', methods=['GET', 'POST'])
def new(user_username):
    next = url_for('shelfBP.new', user_username=user_username)
    up_level_url = url_for('shelfBP.listing', user_username=user_username)

    if not session.get('user_username'):
        flash(u'You must be logged in to create.', u'notice')
        return redirect(url_for('userBP.login', next=next))

    post_inputs={}
    post_errors={}

    if request.method == 'POST':

        post_inputs, post_errors, success_url, new_shelf_details = shelf_form_proc.process_form_shelf_new()

        if success_url:
            return redirect(success_url)


    return render_sidebar_template('mods/shelf/new.html',
                           next=next,
                           up_level_url=up_level_url,
                           post_inputs=post_inputs,
                           post_errors=post_errors
        )



@shelfBP.route('/u/<user_username>/shelves/', methods=['GET', 'POST'])
def listing(user_username):
    # verify the user at this url exists
    if session.get('user_username') != user_username:
        user_exists = user_util.check_if_user_exists(user_username)
        if not user_exists:
            abort(404)

    up_level_url = url_for('userBP.profile', user_username=user_username)

    # verify this is an existing user or redirect to frontpage with flash msg

    if request.method == 'POST':

        if session.get('user_username') != user_username:
            flash(u'You are not authorized to POST here.', u'notice')
            return redirect(url_for('shelfBP.listing',
                                    user_username=user_username))

        process_pin_updates = shelf_form_proc.process_form_shelf_listing_pin_and_unpin()


    shelf_list = shelf_util.get_shelf_listing_full_by_username(user_username)


    return render_sidebar_template('mods/shelf/listing.html',
                            up_level_url=up_level_url,
                            shelf_list=shelf_list,
                            user_username=user_username,
                            )



@shelfBP.route('/u/<user_username>/s/<shelf_slug>/', methods=['GET', 'POST'])
def details(user_username, shelf_slug):
    up_level_url = url_for('shelfBP.listing', user_username=user_username)

    if request.method == 'POST':

        if session.get('user_username') != user_username:
            flash(u'You are not authorized to POST here.', u'notice')
            return redirect(url_for('bookBP.details',
                                    user_username=user_username))

        process_pin_updates = book_form_proc.process_book_listing_form_submission_to_pin_and_unpin()

    shelf_details = shelf_util.get_shelf_details_by_username_and_shelf_slug(user_username, shelf_slug)

    shelf_book_list = book_util.get_shelfs_book_listing_by_username_and_shelf_slug_sorted_alpha(user_username, shelf_slug)

    return render_sidebar_template('mods/shelf/details.html',
                            up_level_url=up_level_url,
                            shelf_details=shelf_details,
                            shelf_book_list=shelf_book_list
                            )



@shelfBP.route('/u/<user_username>/s/<shelf_slug>/edit/', methods=['GET', 'POST'])
def edit(user_username, shelf_slug):
    next = url_for('shelfBP.edit', user_username=user_username, shelf_slug=shelf_slug)
    up_level_url = url_for('shelfBP.listing', user_username=user_username)

    if session.get('user_username') != user_username:
        flash(u'You are not the owner of this shelf.', u'notice')
        return redirect(url_for('shelfBP.details',
                                user_username=user_username,
                                shelf_slug=shelf_slug))
    else:

        original_shelf_details = shelf_util.get_shelf_details_by_username_and_shelf_slug(user_username, shelf_slug)

        post_inputs={}
        post_errors={}

        # create a copies of the original attributes for the post inputs. if the post_inputs
        # points directly to the originals, then modifying the post_inputs will also modify
        # the originals
        post_inputs['shelf_name'] = copy.copy(original_shelf_details.name)
        post_inputs['attributes'] = copy.copy(original_shelf_details.attributes)

        if request.method == 'POST':

            # if spam field wasn't empty, redirect to wherever
            # if request.form.get('email') != None:
            #     flash(u'You have been flagged as a spam bot.', u'notice')
            #     return redirect(url_for('frontpageBP.index'))

            redirect_url, post_inputs, post_errors = shelf_form_proc.process_form_shelf_edit(original_shelf_details, post_inputs, post_errors)

            if redirect_url:
                return redirect(redirect_url)


    return render_sidebar_template('mods/shelf/edit.html',
                           next=next,
                           up_level_url=up_level_url,
                           post_inputs=post_inputs,
                           post_errors=post_errors,
                           shelf_details=original_shelf_details
        )


@shelfBP.route('/u/<user_username>/s/<shelf_slug>/delete/', methods=['GET', 'POST'])
def delete(user_username, shelf_slug):
    up_level_url = url_for('shelfBP.listing', user_username=user_username)

    if session.get('user_username') != user_username:
        flash(u'You are not the owner of this shelf.', u'notice')
        return redirect(url_for('shelfBP.details',
                                user_username=user_username,
                                shelf_slug=shelf_slug))
    else:

        shelf_details = shelf_util.get_shelf_details_by_username_and_shelf_slug(user_username,
                                                                                shelf_slug)

        if request.method == 'POST':

            del_op.delete_shelf_and_books_and_jots_by_shelf_details(shelf_details)

            return redirect(url_for('shelfBP.listing',
                                user_username=user_username))

        return render_sidebar_template('mods/shelf/delete.html',
                                        up_level_url=up_level_url,
                                        shelf_details=shelf_details)