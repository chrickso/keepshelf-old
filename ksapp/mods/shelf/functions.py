# -*- coding: utf-8 -*-

from flask import flash, redirect, request, session, url_for

from ksapp.extensions import db
from ksapp.etc.models.shelf_model import ShelfDB

from ksapp.etc.cache.updates import CacheUpdates
from ksapp.etc.activity.updates import ActivityUpdates

from ksapp.mods.shelf.utils import ShelfUtils
from ksapp.mods.book.utils import BookUtils

from ksapp.etc.utils.strings import StringFunctions

from ksapp.etc.constraints import Constraints
from ksapp.etc.messages import Messages

from ksapp.etc.utils.slug import slugify

# # from pbapp.mods.etc.cache.setup import cache
# from pbapp.mods.etc.cache.keys import CacheKeys

# from pbapp.mods.etc.utils.post_utils import PostUtils

# from pbapp.extensions import redis


# cachekey = CacheKeys()
# post_util = PostUtils()

cache_update = CacheUpdates()
activity_update = ActivityUpdates()

shelf_util = ShelfUtils()
book_util = BookUtils()

string_func = StringFunctions()

constraints = Constraints()
messages = Messages()




class ShelfFunctions(object):

    def __init__(self):
        pass


    def db_insert_new_shelf_plus_updates(self,
                                         shelf_name,
                                         base_slug,
                                         actual_slug,
                                         pin_state,
                                         visibility=None,
                                         user_id=None,
                                         username=None):

        if user_id == None:
            user_id = session['user_id']

        if username == None:
            username == session['user_username']

        new_shelf_details = shelf_util.db_insert_new_shelf(shelf_name, base_slug, actual_slug, pin_state, visibility, user_id, username)

        cache_update.user_creates_new_shelf(username, new_shelf_details)
        activity_update.user_creates_a_shelf(new_shelf_details)

        return new_shelf_details


    def edit_shelf_details_add_1_to_book_count_and_update_caches(self, username, shelf_slug):
        # perform the database update
        updated_shelf_details = shelf_util.edit_shelf_details_add_1_to_book_count(username, shelf_slug)

        # perform the cache update
        cache_update.shelf_stat_updated(updated_shelf_details)

        return updated_shelf_details


    def edit_shelf_details_subtract_1_from_book_count_and_update_caches(self, username, shelf_slug):
        # perform the database update
        updated_shelf_details = shelf_util.edit_shelf_details_subtract_1_from_book_count(username, shelf_slug)

        # perform the cache update
        cache_update.shelf_stat_updated(updated_shelf_details)

        return updated_shelf_details


    def lookup_does_shelf_exist_for_username_and_shelf_slug(self,
                                                            username,
                                                            base_slug):
        shelf_status = shelf_util.count_shelves_matching_username_and_slug(username, base_slug)
        if shelf_status == 1: #messy but utilizes utility for adding numbers to slugs
            return False
        elif shelf_status >1:
            return True



    







    


