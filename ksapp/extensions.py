# -*- coding: utf-8 -*-

from flask.ext.sqlalchemy import SQLAlchemy
db = SQLAlchemy()

# from flask.ext.mail import Mail
# mail = Mail()

from flaskext.bcrypt import Bcrypt
bcrypt = Bcrypt()

# from flask_sijax import Sijax
# sijax = Sijax()

from flask.ext.seasurf import SeaSurf
csrf = SeaSurf()

from flask.ext.redis import Redis
redis = Redis()

from flask.ext.misaka import Misaka
misaka = Misaka()