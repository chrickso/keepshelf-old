# -*- coding: utf-8 -*-

import os, sys

from flask.ext.script import Manager, prompt, prompt_pass, prompt_bool

from ksapp import create_app
from ksapp.extensions import db, redis

from ksapp.etc.models.shelf_model import ShelfDB

# from ksapp.mods.etc.models.user_model import UserDB

from ksapp.etc.utils.reset import create_default_users

from ksapp.etc.cache.setup import cache
from ksapp.etc.dev.dev_jots import dev_jots


# for dev_jots import
from ksapp.etc.utils.slug import slugify
from ksapp.mods.shelf.functions import ShelfFunctions
from ksapp.mods.book.functions import BookFunctions
from ksapp.mods.jot.functions import JotFunctions
shelf_func = ShelfFunctions()
book_func = BookFunctions()
jot_func = JotFunctions()



manager = Manager(create_app())

from ksapp import create_app
app = create_app()
project_root_path = os.path.join(os.path.dirname(app.root_path))



@manager.command
def run():
    """Run local server."""

    # dev
    app.run(host='0.0.0.0')


@manager.command
def prod_run():
    """Run local server."""

    # # # heroku
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)


@manager.command
def cache_reset():
    cache.flush_all() # memcached
    redis.flushall() # redis


@manager.command
def update():
    """Create new table or update columns of existing ones."""
    db.create_all()


@manager.command
def reset():
    """Reset database."""

    # wipe existing DB's & memcached
    cache.flush_all() # memcached
    redis.flushall() # redis
    db.drop_all()
    db.create_all()


    # # insert a test jots
    # jot = PostDB(
    #             poster_username='tester',
    #             poster_id=3,
    #             poster_displayname='tester',
    #             poster_ip='192.168.1.1',
    #             post_title='Traumatic Experiences Support',
    #             post_description="""this site is going to be amazing! can't wait till it's working""",
    #             place_title='Alabama',
    #             board_name='Comments',
    #             place_url='alabama',
    #             board_url='comments',
    #             board_parent_url='general',
    #             pin_status=0,
    #             active=1
    #             )
    # db.session.add(jot)



    # # insert test database entries
    create_default_users()

    ## insert all dev jots into database
    for entry in dev_jots:
        shelf_slug = slugify(entry.shelf)
        book_slug = slugify(entry.book)
        #verify the needed shelf exists and add it if not
        shelf_exists_check = shelf_func.lookup_does_shelf_exist_for_username_and_shelf_slug("chrickso", shelf_slug)
        if not shelf_exists_check:
            shelf_insert_status = shelf_func.db_insert_new_shelf_plus_updates(shelf_name=entry.shelf,
                                                                              base_slug=shelf_slug,
                                                                              actual_slug=shelf_slug,
                                                                              pin_state="0",
                                                                              user_id=1,
                                                                              username="chrickso")

        #verify the needed book exists and add it if not
        book_exists_check = book_func.lookup_does_book_exist_for_username_and_shelf_slug_and_book_slug("chrickso", shelf_slug, book_slug)
        if not book_exists_check:
            book_insert_status = book_func.insert_new_book_into_db(entry.book,
                                                                   shelf_slug,
                                                                   book_slug,
                                                                   book_slug,
                                                                   user_id=1,
                                                                   user_username="chrickso")

        # insert the jot entry
        jot_entry = jot_func.insert_new_jot_into_db(
                                                    shelf_slug=shelf_slug,
                                                    book_slug=book_slug,
                                                    message=entry.body,
                                                    attachments=entry.attachments,
                                                    user_username="chrickso",
                                                    user_id=1)
    db.session.commit()



manager.add_option('-c', '--config',
                   dest="config",
                   required=False,
                   help="config file")


@manager.command
def alembic():
    " Alembic migration utils. "

    from flask import current_app
    from alembic.config import main
    from os import path as op

    global ARGV

    # config = op.join(op.dirname(__file__), 'alembic', 'develop.ini' if current_app.debug else 'production.ini')
    config = op.join(op.dirname(__file__), 'alembic', 'alembic.ini')

    ARGV = ['-c', config] + ARGV

    main(ARGV)



## Needed for alembic:

# ARGV = []

# if __name__ == '__main__':
#     argv = sys.argv[1:]
#     if argv and argv[0] == 'alembic':
#         ARGV = filter(lambda a: not a in ('-c', 'alembic') and not a.startswith('base.config.'), argv)
#         argv = filter(lambda a: not a in ARGV, argv)
#         sys.argv = [sys.argv[0] + ' alembic'] + argv

#     manager.run()


## Comment out if using alembic:

if __name__ == "__main__":
    manager.run()
